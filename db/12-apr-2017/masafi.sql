-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 12, 2017 at 01:08 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `masafi`
--

-- --------------------------------------------------------

--
-- Table structure for table `back_users`
--

CREATE TABLE IF NOT EXISTS `back_users` (
`id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(70) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `token` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `level` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `IP` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `first_name` varchar(25) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(25) CHARACTER SET utf8 NOT NULL,
  `phn` varchar(30) CHARACTER SET utf8 NOT NULL,
  `address` text CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'default.png',
  `joined_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `online_timestamp` varchar(30) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `back_users`
--

INSERT INTO `back_users` (`id`, `email`, `password`, `token`, `level`, `status`, `IP`, `first_name`, `last_name`, `phn`, `address`, `avatar`, `joined_date`, `online_timestamp`) VALUES
(1, 'admin@test.com', '$2a$12$194Ikhvx5AZ9.HijXdjutu2DtnItiLCeo9X8jeEkn.uTh6VHN6j.S', '064ec9c6578e8853c94ed93e475266ad', 2, 1, '', 'Super', 'Admin', '0123456789', 'Dhaka, Bangladesh', 'default.png', '2016-09-27 05:30:48', '1491995071');

-- --------------------------------------------------------

--
-- Table structure for table `common_settings`
--

CREATE TABLE IF NOT EXISTS `common_settings` (
`id` int(11) NOT NULL,
  `logo` varchar(128) NOT NULL,
  `add` text NOT NULL,
  `mail1` varchar(512) NOT NULL,
  `mail2` varchar(512) NOT NULL,
  `phn1` varchar(50) NOT NULL,
  `phn2` varchar(50) NOT NULL,
  `eme_phn` varchar(50) NOT NULL,
  `appoint_phn` varchar(50) NOT NULL,
  `sun_thu` varchar(256) NOT NULL,
  `fri` varchar(256) NOT NULL,
  `sat` varchar(256) NOT NULL,
  `fb` varchar(256) NOT NULL,
  `twt` varchar(256) NOT NULL,
  `link` varchar(256) NOT NULL,
  `copyright` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `common_settings`
--

INSERT INTO `common_settings` (`id`, `logo`, `add`, `mail1`, `mail2`, `phn1`, `phn2`, `eme_phn`, `appoint_phn`, `sun_thu`, `fri`, `sat`, `fb`, `twt`, `link`, `copyright`) VALUES
(1, '8891a2789a101d6c42d32d4e034ec215.png', '1234 Street Name, City Name', 'first_mail@example.com', 'second_mail@example.com', '(800) 123-4567', '(800) 987-4321', '(800) 999-9999', ' +1 874 801 8014', ' 8:30 am to 5:00 pm', 'Closed', '9:30 am to 1:00 pm', 'https://www.facebook.com/', 'https://twitter.com/', 'https://www.linkedin.com/', '© Copyright 2017. All Rights Reserved.');

-- --------------------------------------------------------

--
-- Table structure for table `sister_concern`
--

CREATE TABLE IF NOT EXISTS `sister_concern` (
`id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `short_desc` varchar(512) NOT NULL,
  `long_desc` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `url` varchar(512) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sister_concern`
--

INSERT INTO `sister_concern` (`id`, `name`, `short_desc`, `long_desc`, `image`, `url`) VALUES
(1, 'SHIRIN SPINNING MILLS LTD.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '491238262e54e70235176e2d5aea6816.png', 'shirin_spinning_mills_ltd'),
(2, 'MASAFI BREAD & BISCUIT IND. LTD. ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '149d904fee75823a69770a014cc0f720.jpg', 'masafi_bread_and_biscuit_ind_ltd'),
(3, 'AB.R-SPINNING MILLS LTD.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'f24b5c766c2c4c7db3d23f37aeb8763d.png', 'abr_spinning_mills_ltd'),
(4, 'APON FOOD PRODUCTS LTD.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '0f95ed8d300286218f91bb56f35f5839.png', 'apon_food_products_ltd'),
(5, 'MASAFI FLOUR MILLS LTD.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '28b0b2f0e252db11242b6f05daca27ca.jpg', 'masafi_flour_mills_ltd');

-- --------------------------------------------------------

--
-- Table structure for table `sister_concern_info`
--

CREATE TABLE IF NOT EXISTS `sister_concern_info` (
`id` int(11) NOT NULL,
  `url` varchar(512) NOT NULL,
  `about_desc` text NOT NULL,
  `about_video` varchar(512) NOT NULL,
  `vision_image` varchar(255) NOT NULL,
  `vision_desc` text NOT NULL,
  `mission_image` varchar(255) NOT NULL,
  `mission_desc` text NOT NULL,
  `core_image` varchar(255) NOT NULL,
  `core_desc` text NOT NULL,
  `corporate_image` varchar(255) NOT NULL,
  `corporate_desc` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sister_concern_info`
--

INSERT INTO `sister_concern_info` (`id`, `url`, `about_desc`, `about_video`, `vision_image`, `vision_desc`, `mission_image`, `mission_desc`, `core_image`, `core_desc`, `corporate_image`, `corporate_desc`) VALUES
(1, 'shirin_spinning_mills_ltd', '<p xss=removed>Our Companies’ Core Values of Leadership, Integrity, Respect, Innovation and Continuous Improvement are at the heart \r\n\r\nof everything we do. These Core Values are a source of inspiration as we lead the way to a brighter future for our company and all who depend on \r\n\r\nit.  They support Masafi Groups'' purpose: Making lives better one call at a time, by connecting people to solutions.\r\n                      <br><strong>Leadership   </strong><br>\r\n                      <col xss=removed>We develop leaders... </col> <br>\r\n\r\n                      We foster trust and collaboration to develop leaders focused on sustainable, superior performance.  We set positive examples \r\n\r\nand invest in others so that they can follow the guidelines of leadership.\r\n                      <br>\r\n                      <br><strong>Integrity</strong><br>\r\n                      <col xss=removed>We do the right thing… </col> <br>\r\n                      We conduct our business in accordance with the highest standards of professional behavior and ethics.  We are transparent, \r\n\r\nhonest and ethical in all our interactions with employees, clients, consumers, vendors and the public.\r\n                      <br>\r\n                      <br>\r\n\r\n                    </p>', 'xxx', '1a24e804c39e09ea33a65e9ab179c715.jpg', '<p xss=removed>Our Companies’ Core Values of Leadership, Integrity, Respect, Innovation and Continuous Improvement are at the heart \r\n\r\nof everything we do. These Core Values are a source of inspiration as we lead the way to a brighter future for our company and all who depend on \r\n\r\nit.  They support Masafi Groups'' purpose: Making lives better one call at a time, by connecting people to solutions.\r\n                      <br><strong>Leadership   </strong><br>\r\n                      <col xss=removed>We develop leaders... </col> <br>\r\n\r\n                      We foster trust and collaboration to develop leaders focused on sustainable, superior performance.  We set positive examples \r\n\r\nand invest in others so that they can follow the guidelines of leadership.\r\n                      <br>\r\n                      <br><strong>Integrity</strong><br>\r\n                      <col xss=removed>We do the right thing… </col> <br>\r\n                      We conduct our business in accordance with the highest standards of professional behavior and ethics.  We are transparent, \r\n\r\nhonest and ethical in all our interactions with employees, clients, consumers, vendors and the public.\r\n\r\n                      <br>\r\n                    </p>', '273ba3835c998ce99cf37e5fd79f6761.png', '<p xss=removed>Our Companies’ Core Values of Leadership, Integrity, Respect, Innovation and Continuous Improvement are at the heart \r\n\r\nof everything we do. These Core Values are a source of inspiration as we lead the way to a brighter future for our company and all who depend on \r\n\r\nit.  They support Masafi Groups'' purpose: Making lives better one call at a time, by connecting people to solutions.\r\n                      <br><strong>Leadership   </strong><br>\r\n                      <col xss=removed>We develop leaders... </col> <br>\r\n\r\n                      We foster trust and collaboration to develop leaders focused on sustainable, superior performance.  We set positive examples \r\n\r\nand invest in others so that they can follow the guidelines of leadership.\r\n                      <br>\r\n                      <br><strong>Integrity</strong><br>\r\n                      <col xss=removed>We do the right thing… </col> <br>\r\n                      We conduct our business in accordance with the highest standards of professional behavior and ethics.  We are transparent, \r\n\r\nhonest and ethical in all our interactions with employees, clients, consumers, vendors and the public.\r\n                      <br>\r\n                      <br>\r\n\r\n                      <strong>Respect</strong><br>\r\n                      <col xss=removed>We treat others as we expect to be treated… </col> <br>\r\n                      We embrace each individual’s unique talents and honor diverse life and work styles.  We operate in a spirit of cooperation \r\n\r\nand value human dignity.\r\n                      <br>\r\n                      <br>\r\n\r\n\r\n                      <strong>Innovation</strong><br>\r\n                      <col xss=removed>We anticipate change and shape it to fit our purposes… </col> <br>\r\n                      We acknowledge the weaknesses within our industry and create ethical, forward thinking solutions to overcome them.  We \r\n\r\nidentify, develop and deploy leading edge technology, employee development programs and process improvement tools.\r\n                      <br>\r\n                      <br>\r\n\r\n\r\n                      <strong>Continuous Improvement</strong><br>\r\n                      <col xss=removed>We are a learning organization…</col> <br>\r\n                      We measure, monitor, analyze and improve productivity, processes, tasks and ourselves to satisfy clients and stakeholders.  \r\n\r\nWe work with enthusiasm and intellect, and are driven to surpass what has already been achieved.  We are not afraid to stand alone, especially when \r\n\r\nit is the right thing to do.\r\n                      <br>\r\n                      <br>\r\n                    </p>', '49fc953e7079038a54e743416ac2cd8f.png', '<p xss=removed>Our Companies’ Core Values of Leadership, Integrity, Respect, Innovation and Continuous Improvement are at the heart \r\n\r\nof everything we do. These Core Values are a source of inspiration as we lead the way to a brighter future for our company and all who depend on \r\n\r\nit.  They support Masafi Groups'' purpose: Making lives better one call at a time, by connecting people to solutions.\r\n                      <br><strong>Leadership   </strong><br>\r\n                      <col xss=removed>We develop leaders... </col> <br>\r\n\r\n                      We foster trust and collaboration to develop leaders focused on sustainable, superior performance.  We set positive examples \r\n\r\nand invest in others so that they can follow the guidelines of leadership.\r\n                      <br>\r\n                      <br><strong>Integrity</strong><br>\r\n                      <col xss=removed>We do the right thing… </col> <br>\r\n                      We conduct our business in accordance with the highest standards of professional behavior and ethics.  We are transparent, \r\n\r\n\r\n                      <br>\r\n\r\n\r\n                      <strong>Continuous Improvement</strong><br>\r\n                      <col xss=removed>We are a learning organization…</col> <br>\r\n                      We measure, monitor, analyze and improve productivity, processes, tasks and ourselves to satisfy clients and stakeholders.  \r\n\r\nWe work with enthusiasm and intellect, and are driven to surpass what has already been achieved.  We are not afraid to stand alone, especially when \r\n\r\nit is the right thing to do.\r\n                      <br>\r\n                      <br>\r\n                    </p>', '52cda9d779c5abe7b0408f63f7fd5867.jpg', '<p xss=removed>Our Companies’ Core Values of Leadership, Integrity, Respect, Innovation and Continuous Improvement are at the heart \r\n\r\nof everything we do. These Core Values are a source of inspiration as we lead the way to a brighter future for our company and all who depend on \r\n\r\nit.  They support Masafi Groups'' purpose: Making lives better one call at a time, by connecting people to solutions.\r\n                      <br><strong>Leadership   </strong><br>\r\n\r\n\r\n                      <strong>Respect</strong><br>\r\n                      <col xss=removed>We treat others as we expect to be treated… </col> <br>\r\n                      We embrace each individual’s unique talents and honor diverse life and work styles.  We operate in a spirit of cooperation \r\n\r\nand value human dignity.\r\n                      <br>\r\n                      <br>\r\n\r\n\r\n                      <strong>Innovation</strong><br>\r\n                      <col xss=removed>We anticipate change and shape it to fit our purposes… </col> <br>\r\n                      We acknowledge the weaknesses within our industry and create ethical, forward thinking solutions to overcome them.  We \r\n\r\nidentify, develop and deploy leading edge technology, employee development programs and process improvement tools.\r\n                      <br>\r\n                      <br>\r\n\r\n\r\n                      <strong>Continuous Improvement</strong><br>\r\n                      <col xss=removed>We are a learning organization…</col> <br>\r\n                      We measure, monitor, analyze and improve productivity, processes, tasks and ourselves to satisfy clients and stakeholders.  \r\n\r\nWe work with enthusiasm and intellect, and are driven to surpass what has already been achieved.  We are not afraid to stand alone, especially when \r\n\r\nit is the right thing to do.\r\n                      <br>\r\n                      <br>\r\n                    </p>'),
(2, 'masafi_bread_and_biscuit_ind_ltd', '<p xss=removed>Our Companies’ Core Values of Leadership, Integrity, Respect, Innovation and Continuous Improvement are at the heart \r\n\r\nof everything we do. These Core Values are a source of inspiration as we lead the way to a brighter future for our company and all who depend on \r\n\r\nit.  They support Masafi Groups'' purpose: Making lives better one call at a time, by connecting people to solutions.\r\n                      <br><strong>Leadership   </strong><br>\r\n                      <col xss=removed>We develop leaders... </col> <br>\r\n\r\n                      We foster trust and collaboration to develop leaders focused on sustainable, superior performance.  We set positive examples \r\n\r\nand invest in others so that they can follow the guidelines of leadership.\r\n                      <br>\r\n                      <br><strong>Integrity</strong><br>\r\n                      <col xss=removed>We do the right thing… </col> <br>\r\n                      We conduct our business in accordance with the highest standards of professional behavior and ethics.  We are transparent, \r\n\r\nhonest and ethical in all our interactions with employees, clients, consumers, vendors and the public.\r\n                      <br>\r\n                      <br>\r\n\r\n                    </p>', 'xxx', '85ef2e563db241f5126859f925f47cdc.png', '<p xss=removed>Our Companies’ Core Values of Leadership, Integrity, Respect, Innovation and Continuous Improvement are at the heart \r\n\r\nof everything we do. These Core Values are a source of inspiration as we lead the way to a brighter future for our company and all who depend on \r\n\r\nit.  They support Masafi Groups'' purpose: Making lives better one call at a time, by connecting people to solutions.\r\n                      <br><strong>Leadership   </strong><br>\r\n                      <col xss=removed>We develop leaders... </col> <br>\r\n\r\n                      We foster trust and collaboration to develop leaders focused on sustainable, superior performance.  We set positive examples \r\n\r\nand invest in others so that they can follow the guidelines of leadership.\r\n                      <br>\r\n                      <br><strong>Integrity</strong><br>\r\n                      <col xss=removed>We do the right thing… </col> <br>\r\n                      We conduct our business in accordance with the highest standards of professional behavior and ethics.  We are transparent, \r\n\r\nhonest and ethical in all our interactions with employees, clients, consumers, vendors and the public.\r\n\r\n                      <br>\r\n                    </p>', 'b42aa5cb93093af325f982a995ff0363.png', '<p xss=removed>Our Companies’ Core Values of Leadership, Integrity, Respect, Innovation and Continuous Improvement are at the heart \r\n\r\nof everything we do. These Core Values are a source of inspiration as we lead the way to a brighter future for our company and all who depend on \r\n\r\nit.  They support Masafi Groups'' purpose: Making lives better one call at a time, by connecting people to solutions.\r\n                      <br><strong>Leadership   </strong><br>\r\n                      <col xss=removed>We develop leaders... </col> <br>\r\n\r\n                      We foster trust and collaboration to develop leaders focused on sustainable, superior performance.  We set positive examples \r\n\r\nand invest in others so that they can follow the guidelines of leadership.\r\n                      <br>\r\n                      <br><strong>Integrity</strong><br>\r\n                      <col xss=removed>We do the right thing… </col> <br>\r\n                      We conduct our business in accordance with the highest standards of professional behavior and ethics.  We are transparent, \r\n\r\nhonest and ethical in all our interactions with employees, clients, consumers, vendors and the public.\r\n                      <br>\r\n                      <br>\r\n\r\n                      <strong>Respect</strong><br>\r\n                      <col xss=removed>We treat others as we expect to be treated… </col> <br>\r\n                      We embrace each individual’s unique talents and honor diverse life and work styles.  We operate in a spirit of cooperation \r\n\r\nand value human dignity.\r\n                      <br>\r\n                      <br>\r\n\r\n\r\n                      <strong>Innovation</strong><br>\r\n                      <col xss=removed>We anticipate change and shape it to fit our purposes… </col> <br>\r\n                      We acknowledge the weaknesses within our industry and create ethical, forward thinking solutions to overcome them.  We \r\n\r\nidentify, develop and deploy leading edge technology, employee development programs and process improvement tools.\r\n                      <br>\r\n                      <br>\r\n\r\n\r\n                      <strong>Continuous Improvement</strong><br>\r\n                      <col xss=removed>We are a learning organization…</col> <br>\r\n                      We measure, monitor, analyze and improve productivity, processes, tasks and ourselves to satisfy clients and stakeholders.  \r\n\r\nWe work with enthusiasm and intellect, and are driven to surpass what has already been achieved.  We are not afraid to stand alone, especially when \r\n\r\nit is the right thing to do.\r\n                      <br>\r\n                      <br>\r\n                    </p>', '2d3ff7dc40ba0ef15a0c3387b1c56e72.png', '<p xss=removed>Our Companies’ Core Values of Leadership, Integrity, Respect, Innovation and Continuous Improvement are at the heart \r\n\r\nof everything we do. These Core Values are a source of inspiration as we lead the way to a brighter future for our company and all who depend on \r\n\r\nit.  They support Masafi Groups'' purpose: Making lives better one call at a time, by connecting people to solutions.\r\n                      <br><strong>Leadership   </strong><br>\r\n                      <col xss=removed>We develop leaders... </col> <br>\r\n\r\n                      We foster trust and collaboration to develop leaders focused on sustainable, superior performance.  We set positive examples \r\n\r\nand invest in others so that they can follow the guidelines of leadership.\r\n                      <br>\r\n                      <br><strong>Integrity</strong><br>\r\n                      <col xss=removed>We do the right thing… </col> <br>\r\n                      We conduct our business in accordance with the highest standards of professional behavior and ethics.  We are transparent, \r\n\r\nhonest and ethical in all our interactions with employees, clients, consumers, vendors and the public.\r\n                      <br>\r\n                      <br>\r\n\r\n                      <strong>Respect</strong><br>\r\n                      <col xss=removed>We treat others as we expect to be treated… </col> <br>\r\n                      We embrace each individual’s unique talents and honor diverse life and work styles.  We operate in a spirit of cooperation \r\n\r\nand value human dignity.\r\n                      <br>\r\n                      <br>\r\n\r\n\r\n                      <strong>Innovation</strong><br>\r\n                      <col xss=removed>We anticipate change and shape it to fit our purposes… </col> <br>\r\n                      We acknowledge the weaknesses within our industry and create ethical, forward thinking solutions to overcome them.  We \r\n\r\nidentify, develop and deploy leading edge technology, employee development programs and process improvement tools.\r\n                      <br>\r\n                      <br>\r\n\r\n\r\n                      <strong>Continuous Improvement</strong><br>\r\n                      <col xss=removed>We are a learning organization…</col> <br>\r\n                      We measure, monitor, analyze and improve productivity, processes, tasks and ourselves to satisfy clients and stakeholders.  \r\n\r\nWe work with enthusiasm and intellect, and are driven to surpass what has already been achieved.  We are not afraid to stand alone, especially when \r\n\r\nit is the right thing to do.\r\n                      <br>\r\n                      <br>\r\n                    </p>', '95197d065daa69b7a4b0a97b80b4e8a2.png', '<p xss=removed>Our Companies’ Core Values of Leadership, Integrity, Respect, Innovation and Continuous Improvement are at the heart \r\n\r\nof everything we do. These Core Values are a source of inspiration as we lead the way to a brighter future for our company and all who depend on \r\n\r\nit.  They support Masafi Groups'' purpose: Making lives better one call at a time, by connecting people to solutions.\r\n                      <br><strong>Leadership   </strong><br>\r\n                      <col xss=removed>We develop leaders... </col> <br>\r\n\r\n                      We foster trust and collaboration to develop leaders focused on sustainable, superior performance.  We set positive examples \r\n\r\nand invest in others so that they can follow the guidelines of leadership.\r\n                      <br>\r\n                      <br><strong>Integrity</strong><br>\r\n                      <col xss=removed>We do the right thing… </col> <br>\r\n                      We conduct our business in accordance with the highest standards of professional behavior and ethics.  We are transparent, \r\n\r\nhonest and ethical in all our interactions with employees, clients, consumers, vendors and the public.\r\n                      <br>\r\n                      <br>\r\n\r\n                      <strong>Respect</strong><br>\r\n                      <col xss=removed>We treat others as we expect to be treated… </col> <br>\r\n                      We embrace each individual’s unique talents and honor diverse life and work styles.  We operate in a spirit of cooperation \r\n\r\nand value human dignity.\r\n                      <br>\r\n                      <br>\r\n\r\n\r\n                      <strong>Innovation</strong><br>\r\n                      <col xss=removed>We anticipate change and shape it to fit our purposes… </col> <br>\r\n                      We acknowledge the weaknesses within our industry and create ethical, forward thinking solutions to overcome them.  We \r\n\r\nidentify, develop and deploy leading edge technology, employee development programs and process improvement tools.\r\n                      <br>\r\n                      <br>\r\n\r\n\r\n                      <strong>Continuous Improvement</strong><br>\r\n                      <col xss=removed>We are a learning organization…</col> <br>\r\n                      We measure, monitor, analyze and improve productivity, processes, tasks and ourselves to satisfy clients and stakeholders.  \r\n\r\nWe work with enthusiasm and intellect, and are driven to surpass what has already been achieved.  We are not afraid to stand alone, especially when \r\n\r\nit is the right thing to do.\r\n                      <br>\r\n                      <br>\r\n                    </p>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `back_users`
--
ALTER TABLE `back_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `common_settings`
--
ALTER TABLE `common_settings`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sister_concern`
--
ALTER TABLE `sister_concern`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sister_concern_info`
--
ALTER TABLE `sister_concern_info`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `back_users`
--
ALTER TABLE `back_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `common_settings`
--
ALTER TABLE `common_settings`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sister_concern`
--
ALTER TABLE `sister_concern`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sister_concern_info`
--
ALTER TABLE `sister_concern_info`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
