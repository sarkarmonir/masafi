<?php  defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Home Class
*/
class Contact_us extends CI_controller {
	
	function __construct(){
        
        parent::__construct();
        $this->load->model('data');

    }

	public function index(){

        $data['title'] = "Contact Uus";
        $data['page'] = "front/contact";
        $data['img'] = "assets/images/background/bg-page-title-1.jpg";
        $this->load->view('front/common/template', $data);
        
    }

}