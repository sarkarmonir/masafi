<?php  defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Home Class
*/
class Under_construction extends CI_controller {
	
	function __construct(){
        
        parent::__construct();
        $this->load->model('data');

    }

	public function index(){

        $data['title'] = "Under Construction";
        $data['page'] = "front/under_construction";
        $data['img'] = "assets/images/background/bg-page-title-1.jpg";
        $this->load->view('front/common/template', $data);
        
    }

}