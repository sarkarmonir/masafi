<?php  defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Home Class
*/
class About_us extends CI_controller {
	
	function __construct(){
        
        parent::__construct();
        $this->load->model('data');

    }

	public function about_masafi_group(){

        $data['title'] = "About Masafi Group";
        $data['img'] = "assets/images/background/bg-page-title-1.jpg";
        $data['page'] = "front/about_us/about";
        $this->load->view('front/common/template', $data);
        
    }

    public function message_from_chairman(){

        $data['title'] = "Message from chairman";
        $data['img'] = "assets/images/background/bg-page-title-1.jpg";
        $data['page'] = "front/about_us/chairman_msg";
        $this->load->view('front/common/template', $data);

    }

    public function message_from_md(){

        $data['title'] = "Message from MD";
        $data['img'] = "assets/images/background/bg-page-title-1.jpg";
        $data['page'] = "front/about_us/md_msg";
        $this->load->view('front/common/template', $data);

    }

    public function message_from_ceo(){

        $data['title'] = "Message from C.E.O";
        $data['img'] = "assets/images/background/bg-page-title-1.jpg";
        $data['page'] = "front/about_us/ceo_msg";
        $this->load->view('front/common/template', $data);

    }

    public function leadership(){

        $data['title'] = "Leadership";
        $data['img'] = "assets/images/background/bg-page-title-1.jpg";
        $data['page'] = "front/about_us/org";
        $this->load->view('front/common/template', $data);

    }





   


}