<?php  defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Home Class
*/
class Home extends CI_controller {
	
	function __construct(){

        //$this->sister_concern_table = 'sister_concern';

        parent::__construct();
        $this->load->model('data');

    }

	public function index(){

        $data['title'] = "Home";
        
        $this->load->view('front/common/header', $data);
        $this->load->view('front/pages/home', $data);
        $this->load->view('front/common/footer', $data);
        
    }

    public function services(){

        $data['lists'] = $this->data->getall($this->services_table);
        $data['title'] = "Services";
        $data['page'] = "front/pages/services";
        $this->load->view('front/common/template', $data);

    }

    public function services_details($id){

        $data['list'] = $this->data->getone($this->services_table, $id);
        $data['title'] = "Services Details";
        $data['page'] = "front/pages/services_details";
        $this->load->view('front/common/template', $data);

    }

    public function departments(){

        $data['lists'] = $this->data->getall($this->dept_table);
        $data['title'] = "Departments";
        $data['page'] = "front/pages/departments";
        $this->load->view('front/common/template', $data);

    }

    public function departments_details($id){

        $data['list'] = $this->data->getone($this->dept_table, $id);
        $data['title'] = "Departments Details";
        $data['page'] = "front/pages/departments_details";
        $this->load->view('front/common/template', $data);

    }

    public function doctors(){

        $data['depts'] = $this->data->getall($this->dept_table);
        $data['doctors'] = $this->data->getall($this->doctor_table);
        $data['title'] = "Doctors";
        $data['page'] = "front/pages/doctors";
        $this->load->view('front/common/template', $data);

    }

    public function doctor_profile($id){

        $data['list'] = $this->data->getone($this->doctor_table, $id);
        $data['title'] = "Doctors Profile";
        $data['page'] = "front/pages/doctor_profile";
        $this->load->view('front/common/template', $data);

    }

    public function resource(){

        $data['title'] = "Resource";
        $data['page'] = "front/pages/resource";
        $this->load->view('front/common/template', $data);

    }

    public function faqs(){

        $data['faqs'] = $this->data->getall($this->faqs_table);
        $data['title'] = "Faqs";
        $data['page'] = "front/pages/faqs";
        $this->load->view('front/common/template', $data);

    }

    public function contact(){

        $data['title'] = "Contact";
        $data['page'] = "front/pages/contact";
        $this->load->view('front/common/template', $data);

    }


}