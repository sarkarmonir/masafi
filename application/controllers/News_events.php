<?php  defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Home Class
*/
class News_events extends CI_controller {
	
	function __construct(){
        
        parent::__construct();
        $this->load->model('data');

    }

	public function index(){

        $data['title'] = "News & events";
        $data['page'] = "front/news";
        $data['img'] = "assets/images/background/bg-page-title-1.jpg";
        $this->load->view('front/common/template', $data);
        
    }

    public function details(){

        $data['title'] = "News & events Details";
        $data['page'] = "front/news_details";
        $data['img'] = "assets/images/background/bg-page-title-1.jpg";
        $this->load->view('front/common/template', $data);
        
    }

}