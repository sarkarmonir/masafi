<?php  defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Home Class
*/
class Sister_concern extends CI_controller {
	
	function __construct(){
        
        parent::__construct();

        $this->load->model('sister_concern_info_model');

    }

    public function index(){

        echo "hello";

    }

    public function details($url){

        if (empty($url))
            redirect(site_url());

        // check is it exist url
        $url = $this->sister_concern_info_model->check_exist_url($url);

        if(!empty($url)){

            // get details by url
            $data['list'] = $this->sister_concern_info_model->get_details_by_url($url->url);

            $data['title'] = "edit title"; // $list->title

            $data['img'] = "assets/images/background/bg-page-title-1.jpg";

            $data['page'] = "front/sister_concern_details";

            $this->load->view('front/common/template', $data);

        } else {

            // 404 load here
            echo "404 not found!";

        }
        
    }



}