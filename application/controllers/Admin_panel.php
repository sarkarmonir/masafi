<?php
/**
 * @author -------- <--------@gmail.com>
 * 
 * @copyright  Copyright (C) 2017 rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This controller used for customer maintains
 * @package customer
 * @author --------
 */
class Admin_panel extends CI_Controller {

    /**
     * This is the constructor method
     * @author --------
     */
    function __construct() {

        parent :: __construct();

        $this->load->model('data');


        $this->sister_concern_table = 'sister_concern';
        $this->sister_concern_path = './assets/images/sister_concern/';
        $this->load->model('sister_concern_model');

        $this->sister_concern_info_table = 'sister_concern_info';
        $this->sister_concern_info_path = './assets/images/sister_concern_info/';
        $this->load->model('sister_concern_info_model');

        $this->com_settings_table = 'common_settings';
        $this->com_settings_path = './assets/img/settings/';
        $this->load->model('common_settings');


        

        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');

        if (!$this->back_user->loggedin) {
            redirect(site_url('login'));
        }

    }

    /**
     * This method display all customers with tree based 
     * @author --------
     * @package --------
     * 
     * 
     */
    public function index() {

        redirect(site_url('admin_panel/common_settings'));

    }

    // ##############################################################################################
    //  slider section
    // ##############################################################################################

    /**
     * this function use for add master product and view come from product/add
     * @author --------
     * 
     */
    public function sister_concern() {

        $data['lists'] = $this->data->getall($this->sister_concern_table);
        
        $data['title'] = "Sister Concern";

        $data['page'] = "back/sister_concern/sister_concern";

        $this->load->view('back/common/template', $data);
    }

    /**
     * this function use for add master product and view come from product/add
     * @author --------
     * 
     */
    public function add_sister_concern() {
        if($_POST){

            if ($_FILES['image']['name'] != '') {

                $image_upload = $this->do_upload('image', $this->sister_concern_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed');

                    redirect(site_url('admin_panel/sister_concern'));

                } else {

                    $image = $image_upload["file_name"];

                }

            }
            $id = $this->sister_concern_model->add($image);

            if ($id) {
                
                $this->session->set_flashdata('success', 'Added Successfully.');

                redirect(site_url('admin_panel/sister_concern'));

            } else {
                
                $this->session->set_flashdata('danger', 'Not Added Successfully');

                redirect(site_url('admin_panel/sister_concern'));

            }

        } else {

            redirect(site_url('admin_panel/sister_concern'));

        }
    }

    /**
     * this function use for update edit data process
     * @author ----------
     * @package ----------
     * @return ----------
     */    public function edit_sister_concern($id) {
        
        if(empty($id)){

            redirect(site_url('admin_panel/sister_concern'));

        }

        $data['list'] = $this->data->getone($this->sister_concern_table, $id);
        
        $data['title'] = "Edit Sister Concern";

        $data['page'] = "back/sister_concern//edit_sister_concern";

        $this->load->view('back/common/template', $data);

    }

    /**
     * this function use for update edit data process
     * @author ----------
     * @package ----------
     * @return ----------
     */
    public function do_edit_sister_concern() {

        if($_POST){

            $id = $this->input->post('id');

            if ($_FILES['image']['name'] != '') {

                $del_image = $this->input->post('img');

                $filename = $this->sister_concern_path . $del_image;

                if (file_exists($filename)) {

                    unlink($filename);

                }

                $image_upload = $this->do_upload('image', $this->sister_concern_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed!');

                    redirect(site_url('admin_panel/sister_concern'));

                } else {

                    $image = $image_upload["file_name"];

                }

            } else {

                $image = $this->input->post('img');

            }

            if ($this->sister_concern_model->edit($id, $image)) {

                $this->session->set_flashdata('success', 'Update Successfully.');

                redirect('admin_panel/sister_concern');

            }

        } else {

            redirect('admin_panel/sister_concern', 'refresh');

        }

    }

    /**
     * this method use for delete sister_concern
     * @author ----------
     * @package ----------
     * @return ----------
     */
    public function delete_sister_concern($id, $image) {

        $result = $this->delete_model($this->sister_concern_table, $id, $image, $this->sister_concern_path);

        if($result){

            $this->session->set_flashdata('success', 'Delete Successfully.');

            redirect('admin_panel/sister_concern');

        } else{

            $this->session->set_flashdata('danger', 'Not Delete Successfully.');

            redirect('admin_panel/sister_concern');

        }

    }

    // ##############################################################################################
    //  sister concern info
    // ##############################################################################################

    /**
     * this function use for add master product and view come from product/add
     * @author --------
     * 
     */
    public function sister_concern_info() {

        $data['concerns'] = $this->data->getall($this->sister_concern_table);

        $data['lists'] = $this->data->getall($this->sister_concern_info_table);

        $data['title'] = "Sister Concern Info.";

        $data['page'] = "back/sister_concern_info/sister_concern_info";

        $this->load->view('back/common/template', $data);
    }

    /**
     * this function use for add master product and view come from product/add
     * @author --------
     * 
     */
    public function add_sister_concern_info() {

        if($_POST){

            // vision_image
            if ($_FILES['vision_image']['name'] != '') {

                $image_upload = $this->do_upload('vision_image', $this->sister_concern_info_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed');

                    redirect(site_url('admin_panel/sister_concern_info'));

                } else {

                    $vision_image = $image_upload["file_name"];

                }

            }

            // mission_image
            if ($_FILES['mission_image']['name'] != '') {

                $image_upload = $this->do_upload('mission_image', $this->sister_concern_info_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed');

                    redirect(site_url('admin_panel/sister_concern_info'));

                } else {

                    $mission_image = $image_upload["file_name"];

                }

            }

            // core_image
            if ($_FILES['core_image']['name'] != '') {

                $image_upload = $this->do_upload('core_image', $this->sister_concern_info_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed');

                    redirect(site_url('admin_panel/sister_concern_info'));

                } else {

                    $core_image = $image_upload["file_name"];

                }

            }

            // corporate_image
            if ($_FILES['corporate_image']['name'] != '') {

                $image_upload = $this->do_upload('corporate_image', $this->sister_concern_info_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed');

                    redirect(site_url('admin_panel/sister_concern_info'));

                } else {

                    $corporate_image = $image_upload["file_name"];

                }

            }

            $id = $this->sister_concern_info_model->add($vision_image, $mission_image, $core_image, $corporate_image);

            if ($id) {
                
                $this->session->set_flashdata('success', 'Added Successfully.');

                redirect(site_url('admin_panel/sister_concern_info'));

            } else {
                
                $this->session->set_flashdata('danger', 'Not Added Successfully');

                redirect(site_url('admin_panel/sister_concern_info'));

            }

        } else {

            redirect(site_url('admin_panel/sister_concern_info'));

        }

    }

    /**
     * this function use for update edit data process
     * @author ----------
     * @package ----------
     * @return ----------
     */
    public function edit_sister_concern_info($id) {
        
        if(empty($id)){

            redirect(site_url('admin_panel/sister_concern_info'));

        }

        $data['concerns'] = $this->data->getall($this->sister_concern_table);

        $data['list'] = $this->data->getone($this->sister_concern_info_table, $id);

        $data['title'] = "Edit Sister Concern";

        $data['page'] = "back/sister_concern_info/edit_sister_concern_info";

        $this->load->view('back/common/template', $data);

    }

    /**
     * this function use for update edit data process
     * @author ----------
     * @package ----------
     * @return ----------
     */
    public function do_edit_sister_concern_info() {

        if($_POST){
            
            $id = $this->input->post('id');

            // $vision_image
            if ($_FILES['vision_image']['name'] != '') {

                $del_image = $this->input->post('vision_img');

                $filename = $this->sister_concern_info_path . $del_image;

                if (file_exists($filename)) {

                    unlink($filename);

                }

                $image_upload = $this->do_upload('vision_image', $this->sister_concern_info_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed!');

                    redirect(site_url('admin_panel/sister_concern_info'));

                } else {

                    $vision_image = $image_upload["file_name"];

                }

            } else {

                $vision_image = $this->input->post('vision_img');

            }

            // $mission_image
            if ($_FILES['mission_image']['name'] != '') {

                $del_image = $this->input->post('mission_img');

                $filename = $this->sister_concern_info_path . $del_image;

                if (file_exists($filename)) {

                    unlink($filename);

                }

                $image_upload = $this->do_upload('mission_image', $this->sister_concern_info_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed!');

                    redirect(site_url('admin_panel/sister_concern_info'));

                } else {

                    $mission_image = $image_upload["file_name"];

                }

            } else {

                $mission_image = $this->input->post('mission_img');

            }


            // $core_image
            if ($_FILES['core_image']['name'] != '') {

                $del_image = $this->input->post('core_img');

                $filename = $this->sister_concern_info_path . $del_image;

                if (file_exists($filename)) {

                    unlink($filename);

                }

                $image_upload = $this->do_upload('core_image', $this->sister_concern_info_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed!');

                    redirect(site_url('admin_panel/sister_concern_info'));

                } else {

                    $core_image = $image_upload["file_name"];

                }

            } else {

                $core_image = $this->input->post('core_img');

            }

            // $corporate_image
            if ($_FILES['corporate_image']['name'] != '') {

                $del_image = $this->input->post('corporate_img');

                $filename = $this->sister_concern_info_path . $del_image;

                if (file_exists($filename)) {

                    unlink($filename);

                }

                $image_upload = $this->do_upload('corporate_image', $this->sister_concern_info_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed!');

                    redirect(site_url('admin_panel/sister_concern_info'));

                } else {

                    $corporate_image = $image_upload["file_name"];

                }

            } else {

                $corporate_image = $this->input->post('corporate_img');

            }

        if ($this->sister_concern_info_model->edit($id, $vision_image, $mission_image, $core_image, $corporate_image)) {

                $this->session->set_flashdata('success', 'Update Successfully.');

                redirect('admin_panel/sister_concern_info');

            }

        } else {

            redirect('admin_panel/sister_concern_info', 'refresh');

        }

    }

    /**
     * this method use for delete sister_concern
     * @author ----------
     * @package ----------
     * @return ----------
     */
    public function delete_sister_concern_info($id, $vision_image, $mission_image, $core_image, $corporate_image) {

        $result = $this->sister_con_delete_model($this->sister_concern_info_table, $id, $vision_image, $mission_image, $core_image, $corporate_image, $this->sister_concern_info_path);

        if($result){

            $this->session->set_flashdata('success', 'Delete Successfully.');

            redirect('admin_panel/sister_concern_info');

        } else{

            $this->session->set_flashdata('danger', 'Not Delete Successfully.');

            redirect('admin_panel/sister_concern_info');

        }

    }

    // ##############################################################################################
    //  Common settings section
    // ##############################################################################################

    /**
     * this function use for add master product and view come from product/add
     * @author --------
     * 
     */
    public function common_settings() {

        $data['list'] = $this->data->getall($this->com_settings_table);
        
        $data['title'] = "Common Settings";

        $data['page'] = "back/settings/common_settings";

        $this->load->view('back/common/template', $data);
    }

    
    /**
     * this function use for update edit data process
     * @author ----------
     * @package ----------
     * @return ----------
     */
    public function do_edit_common_settings() {

        if($_POST){

            $id = $this->input->post('id');
            if ($_FILES['image']['name'] != '') {

                $del_image = $this->input->post('img');
                $filename = $this->com_settings_path . $del_image;
                if (file_exists($filename)) {
                    unlink($filename);
                }

                $image_upload = $this->do_upload('image', $this->com_settings_path);

                if ($image_upload == FALSE) {
                    $this->session->set_flashdata('danger', 'Image Upload Failed!');
                    redirect(site_url('admin_panel/common_settings'));
                } else {
                    $image = $image_upload["file_name"];
                }
            } else {
                $image = $this->input->post('img');
            }

            if ($this->common_settings->edit($id, $image)) {
                $this->session->set_flashdata('success', 'Update Successfully.');
                redirect('admin_panel/common_settings');
            }
        } else {

            redirect('admin_panel/common_settings', 'refresh');

        }

    }



















    /**
     * This method has been used to upload file.
     * @author ---------------
     * @param type $field_name
     * @return boolean
     */
    public function do_upload($field_name , $path) {

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = '5120';
        $config['xss_clean'] = TRUE;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($field_name)) {
            return false;
        } else {
            return $this->upload->data();
        }
    }

    /**
     * This method has been used to delete anything
     * @author ---------------
     * @param type $field_name
     * @return boolean
     */
    public function delete($table, $id, $image = NULL, $path = NULL) {

        if($image != NULL){

            $this->data->delete($table, $id);
            $filename = $path . $image;

            if (file_exists($filename)) {
                unlink($filename);
                return TRUE;
            } else {
                return FALSE;
            }

        } else {

            if ($this->data->delete($table, $id)) {
                return TRUE;
            } else {
                return FALSE;
            }

        }
        
    }

    public function sister_con_delete($table, $id, $vision_image = NULL, $mission_image = NULL, $core_image = NULL, $corporate_image = NULL, $path = NULL) {

        $this->data->delete($table, $id);

        $filename1 = $path . $vision_image;
        $filename2 = $path . $mission_image;
        $filename3 = $path . $core_image;
        $filename4 = $path . $corporate_image;


        if (file_exists($filename1)) {

            unlink($filename1);

            if (file_exists($filename2)) {

                unlink($filename2);
                
            }

            if (file_exists($filename3)) {

                unlink($filename3);
                
            }

            if (file_exists($filename4)) {

                unlink($filename4);
                
            }

            return TRUE;


        } else {

            return FALSE;

        }

    }

    /**
     * This method has been used for logout.
     * @author ---------------
     * @param ---
     * @return ---
     */
    public function logout($hash) {

        $config = $this->config->item("cookieprefix");

        $this->load->helper("cookie");
        if ($hash != $this->security->get_csrf_hash()) {

            $data['title'] = "Error";

            $data['page'] = "back/error/admin_error";

            $this->session->set_flashdata('danger', 'CSRF and Cookie problem');

            $this->load->view('back/common/template', $data);
        }
        delete_cookie($config . "un");
        delete_cookie($config . "tkn");
        $this->session->sess_destroy();
        redirect('login');
    }





    





}