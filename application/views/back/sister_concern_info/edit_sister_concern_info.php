<div class="content-wrapper">
    <section class="content-header">
        <h1><?=$title?></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Admin Panel</a></li>
            <li class="active"><?=$title?></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- form row -->
        <?php echo form_open_multipart(site_url("admin_panel/do_edit_sister_concern_info"), array("class" => "form-horizontal")) ?>
        <input type="hidden"  name="id" value="<?php echo $list[0]->id; ?>" />
        <div class="row">
            <div class="col-md-12">
                <!-- box -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Update</h3>
                        <div class="box-tools pull-right">
                            <button type="submit" class="btn btn-success btn-sm">Update</button>
                       </div>
                    </div><!-- /.box-header -->
                    <!-- form -->
                    <div class="box-body">
                        <div class="col-md-12">
                            <!-- name -->
                            <div class="form-group">
                                <label class="col-sm-2 control-label"> Sister Concern Name<sup style="color: red">*</sup></label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="url">
                                        <?php foreach($concerns as $concern) : ?>
                                        <option value="<?=$concern->url?>" <?php if($concern->url == $list[0]->url){ echo "selected"; } ?> ><?=$concern->name?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <!-- Custom Tabs -->
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a aria-expanded="true" href="#about_company" data-toggle="tab">About Company</a></li>
                                    <li class=""><a aria-expanded="false" href="#vision" data-toggle="tab">Vision</a></li>
                                    <li class=""><a aria-expanded="false" href="#mission" data-toggle="tab">Mission</a></li>
                                    <li class=""><a aria-expanded="false" href="#core" data-toggle="tab">Core Values</a></li>
                                    <li class=""><a aria-expanded="false" href="#corporate" data-toggle="tab">Corporate Goals</a></li>
                                </ul>
                                <div class="tab-content">
                                    <!-- about_company tab -->
                                    <div class="tab-pane active" id="about_company">
                                        <!-- desc -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Description<sup style="color: red">*</sup></label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control"  name="about_desc" rows="20" placeholder="Description here" required><?php echo $list[0]->about_desc; ?></textarea>
                                            </div>
                                        </div>
                                        <!-- video link -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Video Link<sup style="color: red">*</sup></label>
                                            <div class="col-sm-10">
                                                <input class="form-control" type="text" placeholder="e.g: youtube embed link here" name="about_video" value="<?php echo $list[0]->about_video; ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <!-- vision tab -->
                                    <div class="tab-pane" id="vision">
                                        <!-- image -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Image<sup style="color: red">*</sup></label>
                                            <div class="col-sm-2">
                                                <input type="file"  name="vision_image"  id="myFile" onchange="vision_imageshow(this);" />
                                                <input type="hidden" name="vision_img" value="<?=$list[0]->vision_image?>" />
                                            </div>
                                            <div class="col col-lg-4">
                                                <img id="vision_preview" src="<?=base_url()?>assets/images/sister_concern_info/<?=$list[0]->vision_image?>" width="130px" />
                                                <script>
                                                    function vision_imageshow(input) {

                                                        if (input.files && input.files[0]) {
                                                            var reader = new FileReader();

                                                            reader.onload = function(e) {
                                                                $('#vision_preview')
                                                                        .attr('src', e.target.result)
                                                                        .width(130);
                                                            }
                                                            reader.readAsDataURL(input.files[0]);

                                                        } else {
                                                            var filename = "";
                                                            filename = "file:\/\/" + input.value;
                                                            document.form2.vision_preview.src = filename;
                                                            document.form2.vision_preview.style.width = "80px";
                                                        }
                                                    }
                                                </script>
                                            </div>
                                        </div>

                                        <!-- desc -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> vision Description<sup style="color: red">*</sup></label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control"  name="vision_desc" rows="20" placeholder="Description here" required><?php echo $list[0]->vision_desc; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- mission tab -->
                                    <div class="tab-pane" id="mission">
                                        <!-- image -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Image<sup style="color: red">*</sup></label>
                                            <div class="col-sm-2">
                                                <input type="file"  name="mission_image"  id="myFile" onchange="mission_imageshow(this);" />
                                                <input type="hidden" name="mission_img" value="<?=$list[0]->mission_image?>" />
                                            </div>
                                            <div class="col col-lg-4">
                                                <img id="mission_preview" src="<?=base_url()?>assets/images/sister_concern_info/<?=$list[0]->mission_image?>" width="130px" />
                                                <script>
                                                    function mission_imageshow(input) {

                                                        if (input.files && input.files[0]) {
                                                            var reader = new FileReader();

                                                            reader.onload = function(e) {
                                                                $('#mission_preview')
                                                                        .attr('src', e.target.result)
                                                                        .width(130);
                                                            }
                                                            reader.readAsDataURL(input.files[0]);

                                                        } else {
                                                            var filename = "";
                                                            filename = "file:\/\/" + input.value;
                                                            document.form2.mission_preview.src = filename;
                                                            document.form2.mission_preview.style.width = "80px";
                                                        }
                                                    }
                                                </script>
                                            </div>
                                        </div>
                                        <!-- desc -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Mission Description<sup style="color: red">*</sup></label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control"  name="mission_desc" rows="20" placeholder="Description here" required><?php echo $list[0]->mission_desc; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- core tab -->
                                    <div class="tab-pane" id="core">
                                        <!-- image -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Image<sup style="color: red">*</sup></label>
                                            <div class="col-sm-2">
                                                <input type="file"  name="core_image"  id="myFile" onchange="core_imageshow(this);" />
                                                <input type="hidden" name="core_img" value="<?=$list[0]->core_image?>" />
                                            </div>
                                            <div class="col col-lg-4">
                                                <img id="core_preview" src="<?=base_url()?>assets/images/sister_concern_info/<?=$list[0]->core_image?>" width="130px" />
                                                <script>
                                                    function core_imageshow(input) {

                                                        if (input.files && input.files[0]) {
                                                            var reader = new FileReader();

                                                            reader.onload = function(e) {
                                                                $('#core_preview')
                                                                        .attr('src', e.target.result)
                                                                        .width(130);
                                                            }
                                                            reader.readAsDataURL(input.files[0]);

                                                        } else {
                                                            var filename = "";
                                                            filename = "file:\/\/" + input.value;
                                                            document.form2.core_preview.src = filename;
                                                            document.form2.core_preview.style.width = "80px";
                                                        }
                                                    }
                                                </script>
                                            </div>
                                        </div>
                                        <!-- desc -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Core Description<sup style="color: red">*</sup></label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control"  name="core_desc" rows="20" placeholder="Description here" required><?php echo $list[0]->core_desc; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- corporate tab -->
                                    <div class="tab-pane" id="corporate">
                                        <!-- image -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Image<sup style="color: red">*</sup></label>
                                            <div class="col-sm-2">
                                                <input type="file"  name="corporate_image"  id="myFile" onchange="corporate_imageshow(this);" />
                                                <input type="hidden" name="corporate_img" value="<?=$list[0]->corporate_image?>" />
                                            </div>
                                            <div class="col col-lg-4">
                                                <img id="corporate_preview" src="<?=base_url()?>assets/images/sister_concern_info/<?=$list[0]->corporate_image?>" width="130px" />
                                                <script>
                                                    function corporate_imageshow(input) {

                                                        if (input.files && input.files[0]) {
                                                            var reader = new FileReader();

                                                            reader.onload = function(e) {
                                                                $('#corporate_preview')
                                                                        .attr('src', e.target.result)
                                                                        .width(130);
                                                            }
                                                            reader.readAsDataURL(input.files[0]);

                                                        } else {
                                                            var filename = "";
                                                            filename = "file:\/\/" + input.value;
                                                            document.form2.corporate_preview.src = filename;
                                                            document.form2.corporate_preview.style.width = "80px";
                                                        }
                                                    }
                                                </script>
                                            </div>
                                        </div>
                                        <!-- desc -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Corporate Description<sup style="color: red">*</sup></label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control"  name="corporate_desc" rows="20" placeholder="Description here" required><?php echo $list[0]->corporate_desc; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.tab-pane -->
                                </div>
                                <!-- /.tab-content -->
                            </div>
                            <!-- nav-tabs-custom -->
                        </div>
                        </div>
                </div><!-- /.box -->
            </div>
        </div>
        <?php echo form_close(); ?>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->