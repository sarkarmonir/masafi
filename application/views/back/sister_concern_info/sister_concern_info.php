<div class="content-wrapper">
    <section class="content-header">
        <h1><?=$title?></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Admin Panel</a></li>
            <li class="active"><?=$title?></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <!-- form row -->
        <?php echo form_open_multipart(site_url("admin_panel/add_sister_concern_info"), array("class" => "form-horizontal")) ?>

        <div class="row">
            <div class="col-md-12">
                <!-- box -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Add <?=$title?></h3>
                        <div class="box-tools pull-right">
                            <button type="submit" class="btn btn-success btn-sm">Add <?=$title?></button>
                       </div>
                    </div><!-- /.box-header -->

                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)) { ?>
                        <!-- notice -->
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fa fa-check"></i><?php echo $this->session->flashdata('success') ?></h5>
                        </div>
                    <?php } ?>
                    <?php $danger = $this->session->flashdata('danger');
                    if (!empty($danger)) { ?>
                        <!-- notice -->
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fa fa-check"></i><?php echo $this->session->flashdata('danger') ?></h5>
                        </div>
                    <?php } ?>

                    <!-- form -->
                    <div class="box-body">
                        <div class="col-md-12">
                            <!-- name -->
                            <div class="form-group">
                                <label class="col-sm-2 control-label"> Sister Concern Name<sup style="color: red">*</sup></label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="url">
                                        <?php foreach($concerns as $concern) : ?>
                                        <option value="<?php echo $concern->url; ?>" ><?php echo $concern->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <!-- Custom Tabs -->
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a aria-expanded="true" href="#about_company" data-toggle="tab">About Company</a></li>
                                    <li class=""><a aria-expanded="false" href="#vision" data-toggle="tab">Vision</a></li>
                                    <li class=""><a aria-expanded="false" href="#mission" data-toggle="tab">Mission</a></li>
                                    <li class=""><a aria-expanded="false" href="#core" data-toggle="tab">Core Values</a></li>
                                    <li class=""><a aria-expanded="false" href="#corporate" data-toggle="tab">Corporate Goals</a></li>
                                </ul>
                                <div class="tab-content">
                                    <!-- about_company tab -->
                                    <div class="tab-pane active" id="about_company">
                                        <!-- desc -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Description<sup style="color: red">*</sup></label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control"  name="about_desc" rows="20" placeholder="Description here" required></textarea>
                                            </div>
                                        </div>
                                        <!-- video link -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Video Link<sup style="color: red">*</sup></label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" placeholder="e.g: youtube embed link here" name="about_video" required>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- vision tab -->
                                    <div class="tab-pane" id="vision">
                                        <!-- image -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Vision Main Image<sup style="color: red">*</sup></label>
                                            <div class="col-sm-8">
                                                <input type="file" name="vision_image" required>
                                            </div>
                                        </div>
                                        <!-- desc -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> vision Description<sup style="color: red">*</sup></label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control"  name="vision_desc" rows="20" placeholder="Description here" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- mission tab -->
                                    <div class="tab-pane" id="mission">
                                        <!-- image -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Mission Main Image<sup style="color: red">*</sup></label>
                                            <div class="col-sm-8">
                                                <input type="file" name="mission_image" required>
                                            </div>
                                        </div>
                                        <!-- desc -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Mission Description<sup style="color: red">*</sup></label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control"  name="mission_desc" rows="20" placeholder="Description here" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- core tab -->
                                    <div class="tab-pane" id="core">
                                        <!-- image -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Core Main Image<sup style="color: red">*</sup></label>
                                            <div class="col-sm-8">
                                                <input type="file" name="core_image" required>
                                            </div>
                                        </div>
                                        <!-- desc -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Core Description<sup style="color: red">*</sup></label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control"  name="core_desc" rows="20" placeholder="Description here" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- corporate tab -->
                                    <div class="tab-pane" id="corporate">
                                        <!-- image -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Corporate Main Image<sup style="color: red">*</sup></label>
                                            <div class="col-sm-8">
                                                <input type="file" name="corporate_image" required>
                                            </div>
                                        </div>
                                        <!-- desc -->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"> Corporate Description<sup style="color: red">*</sup></label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control"  name="corporate_desc" rows="20" placeholder="Description here" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.tab-pane -->
                                </div>
                                <!-- /.tab-content -->
                            </div>
                            <!-- nav-tabs-custom -->
                        </div>
                        </div>
                    
                </div><!-- /.box -->
            </div>
        </div>
        <?php echo form_close(); ?>

        <!-- list row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <!-- box header -->
                    <?php if(empty($lists)) : ?>
                        <div class="box-header">
                            <h3 class="box-title">No List Found!</h3>
                        </div>
                    <?php else: ?>
                    <div class="box-header">
                        <h3 class="box-title"> List</h3>
                    </div>
                    <!-- box content -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody>
                                <tr>
                                    <th>Sl.</th>
                                    <th>Sister Concern Name</th>
                                    <th>Actions</th>
                                </tr>
                                <?php $x = 0; foreach($lists as $list) : $x++; ?>
                                <tr>
                                    <td><?php echo $x; ?></td>
                                    <td>
                                        <?=$list->url?>
                                    </td>
                                    <td>
                                        <a href="<?=site_url('admin_panel/edit_sister_concern_info/' . $list->id) ?>" data-toggle="tooltip" title="Edit" data-original-title="Edit"><i class="fa  fa-file-text"></i></a>
                                        |
                                        <a href="<?=site_url('admin_panel/delete_sister_concern_info/' . $list->id . '/' . $list->vision_image . '/' . $list->mission_image . '/' . $list->core_image . '/' . $list->corporate_image) ?>" onclick="return checkDelete();"><i class="fa fa-trash text-red"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <?php endif; ?>
                </div>  
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->