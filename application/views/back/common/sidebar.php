<!-- Left side column -->
<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <!-- add sister concern -->
            <li class="treeview <?php if ($page == 'back/sister_concern/sister_concern' || $page == 'back/sister_concern_info/sister_concern_info' || $page == 'back/sister_concern_info/edit_sister_concern_info') echo "active"; ?>">
                <a href="#" >
                    <i class="fa fa-fw fa-briefcase"></i>
                    <span>Sister Concern</span>
                    <span class="pull-right-container">
                        <i class="fa  fa-chevron-down pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($page == 'back/sister_concern/sister_concern') echo "active"; ?>" ><a href="<?php echo site_url("admin_panel/sister_concern") ?>" ><i class="fa fa-arrow-right"></i>  Add Sister Concern</a></li>
                    <li class="<?php if ($page == 'back/sister_concern_info/sister_concern_info' || $page == 'back/sister_concern_info/edit_sister_concern_info') echo "active"; ?>" ><a href="<?php echo site_url("admin_panel/sister_concern_info") ?>"><i class="fa fa-arrow-right"></i> Add Sister Concern Info.</a></li>
                </ul>
            </li>
            <!-- common settings -->
            <li class="<?php if ($page == 'back/settings/common_settings') echo "active"; ?>" >
                <a href="<?php echo site_url("admin_panel/common_settings")?>" >
                    <i class="fa fa-fw fa-gears"></i>
                    <span>Common Settings</span>
                </a>
            </li>
        </ul>
    </section>
</aside>