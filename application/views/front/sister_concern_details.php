
<section class="about-section">
  <div class="container">
    <div class="row-fluid">
      <div class="col-md-3">
        <div class="sec-title left-aligned">
          <h2 class="title-opt">Overview</h2></div>
          <div class="about-box-1">
            <nav class="nav-sidebar">
              <ul class="nav tabs">
                <li class="active"><a href="#about" data-toggle="tab">About Company</a></li>

                <li class=""><a href="#vision" data-toggle="tab">Vision</a></li>
                <li class=""><a href="#mission" data-toggle="tab">Mission</a></li>
                <li class=""><a href="#core" data-toggle="tab">Core values</a></li>
                <li class=""><a href="#corporate" data-toggle="tab">Corporate Goal</a></li>

                <li class=""><a href="#organogram" data-toggle="tab">Organogram</a></li>
                <li class=""><a href="#product" data-toggle="tab">Product</a></li>   
                <li class=""><a href="#client" data-toggle="tab">Client</a></li>
              </ul>
            </nav>
          </div>
        </div>
        <div class="col-md-6">
          <div class="">
            <!-- tab content -->
            <div class="tab-content">

              <div class="tab-pane active text-style" id="about">
                <div class="sec-title left-aligned">
                  <h2 class="title-opt">About Company</h2>
                </div>
                <div class="about-box-1">
                  <p><?=$list->about_desc?></p>
                  <hr>
                  <iframe width="100%" height="350" src="https://www.youtube.com/embed/eeik7MuQ930" frameborder="0" allowfullscreen></iframe>
                </div>
              </div>

              <div class="tab-pane text-style" id="vision">
                <div class="sec-title left-aligned">
                  <h2 class="title-opt">Vision</h2></div>
                  <div class="about-box-1">
                    <img class="img img-responsive" src="<?=base_url()?>assets/images/sister_concern_info/<?=$list->vision_image?>">
                    <br>
                    <br>
                    <?=$list->vision_desc?>
                  </div>
                </div>

                <div class="tab-pane text-style" id="mission">
                <div class="sec-title left-aligned">
                  <h2 class="title-opt">Mission</h2></div>
                  <div class="about-box-1">
                    <img class="img img-responsive" src="<?=base_url()?>assets/images/sister_concern_info/<?=$list->mission_image?>">
                    <br>
                    <br>
                    <?=$list->mission_desc?>
                  </div>
                </div>

                <div class="tab-pane text-style" id="core">
                <div class="sec-title left-aligned">
                  <h2 class="title-opt">Core Values</h2></div>
                  <div class="about-box-1">
                    <img class="img img-responsive" src="<?=base_url()?>assets/images/sister_concern_info/<?=$list->core_image?>">
                    <br>
                    <br>
                    <?=$list->core_desc?>
                  </div>
                </div>

                <div class="tab-pane text-style" id="corporate">
                <div class="sec-title left-aligned">
                  <h2 class="title-opt">Corporate Goal</h2></div>
                  <div class="about-box-1">
                    <img class="img img-responsive" src="<?=base_url()?>assets/images/sister_concern_info/<?=$list->corporate_image?>">
                    <br>
                    <br>
                    <?=$list->corporate_desc?>
                  </div>
                </div>

                <div class="tab-pane text-style" id="organogram">
                  <div class="sec-title left-aligned">
                    <h2 class="title-opt">Organogram</h2>
                  </div>
                  <div class="about-box-1 product-sec">
                    <div class="product-item">
                      <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                      <h4>Name <br> Designation</h4>
                    </div>
                    <div class="product-item">
                      <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                      <h4>Name <br> Designation</h4>
                    </div>
                    <div class="product-item">
                      <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                      <h4>Name <br> Designation</h4>
                    </div>
                    <div class="product-item">
                      <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                      <h4>Name <br> Designation</h4>
                    </div>
                    <div class="product-item">
                      <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                      <h4>Name <br> Designation</h4>
                    </div>
                    <div class="product-item">
                      <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                      <h4>Name <br> Designation</h4>
                    </div>
                  </div>
                </div>

                <div class="tab-pane text-style" id="product">
                  <div class="sec-title left-aligned">
                    <h2 class="title-opt">Product</h2></div>
                    <div class="about-box-1 product-sec">
                      <div class="product-item">
                        <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                        <h4>Product Name</h4>
                      </div>
                      <div class="product-item">
                        <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                        <h4>Product Name</h4>
                      </div>
                      <div class="product-item">
                        <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                        <h4>Product Name</h4>
                      </div>
                      <div class="product-item">
                        <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                        <h4>Product Name</h4>
                      </div>
                      <div class="product-item">
                        <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                        <h4>Product Name</h4>
                      </div>
                      <div class="product-item">
                        <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                        <h4>Product Name</h4>
                      </div>
                    </div>
                  </div>

                  <div class="tab-pane text-style" id="client">
                    <div class="sec-title left-aligned">
                      <h2 class="title-opt">Clients</h2></div>
                      <div class="about-box-1 product-sec">
                        <div class="product-item">
                          <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                          <h4>Client Name</h4>
                        </div>
                        <div class="product-item">
                          <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                          <h4>Client Name</h4>
                        </div>
                        <div class="product-item">
                          <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                          <h4>Client Name</h4>
                        </div>
                        <div class="product-item">
                          <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                          <h4>Client Name</h4>
                        </div>
                        <div class="product-item">
                          <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                          <h4>Client Name</h4>
                        </div>
                        <div class="product-item">
                          <img src="<?=base_url()?>assets/images/name.png" class="img-rounded">
                          <h4>Client Name</h4>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="sec-title left-aligned">
                  <h2 class="title-opt">Contact</h2></div>
                  <div class="about-box-1">
                    <img class="img img-responsive" src="<?=base_url()?>assets/images/sis/4.png">
                    <p><strong>Address:</strong> Gulshan, Dhaka-1212</p>
                    <p><strong>City:</strong> Dhaka</p>
                    <p><strong>Country:</strong> Bangladesh</p>
                    <p><strong>Phone:</strong> 01717231363</p>
                    <p><strong>Email:</strong> masafi@masafigroup.com</p>
                    <p><strong>Web:</strong> masafigroup.com</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
