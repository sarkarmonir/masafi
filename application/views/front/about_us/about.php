        <div id="main">
            <section class="generic-heading-3">
                <div class="container">
                    <h1>About Us</h1>
                    <strong class="title-line">conserve the lands and waters on which all life depends</strong> </div>
            </section>

            <section class="about-page">
                <section class="about-section-1">
                    <div class="container">
                        <div class="row-fluid">
                            <div class="col-md-5">
                                <div class="about-box-1">
                                    <h2>Who We are</h2>
                                    <div class="frame">
                                        <a href="#"><img src="<?=base_url()?>assets/images/about/about-who-img-1.jpg" alt="img"></a>
                                    </div>
                                    <div class="text-box"> <strong class="title">Lorem survived not </strong>
                                        <p>Vestibulum id ligula porta consectetur est at lobortis. cferment sed.Vestibulum id ligula porta consectetur est at lobortis. </p>
                                    </div>
                                    <div class="text-box-1">
                                        <p>We has survived not only five centuries, but also the leap electronict publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="about-accordion-box">
                                    <div class="heading-bg">
                                        <h2>Why Choose Us</h2>
                                    </div>
                                    <div class="accordion" id="accordion2">
                                        <div class="accordion-group">
                                            <div class="accordion-heading active"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">We are always improving<col-md-><i class="fa fa-plus"></i></col-md-></a> </div>
                                            <div id="collapseOne" class="accordion-body collapse in">
                                                <div class="accordion-inner">
                                                    <div class="frame">
                                                        <a href="#"><img src="<?=base_url()?>assets/images/about/accordion-img-1.jpg" alt="img"></a>
                                                    </div>
                                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident!</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">Lorem Ipsum Dolorem<col-md-><i class="fa fa-plus"></i></col-md-></a> </div>
                                            <div id="collapseTwo" class="accordion-body collapse">
                                                <div class="accordion-inner">
                                                    <div class="frame">
                                                        <a href="#"><img src="<?=base_url()?>assets/images/about/accordion-img-2.jpg" alt="img"></a>
                                                    </div>
                                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident!</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">Sed ut Perspiciatis<col-md-><i class="fa fa-plus"></i></col-md-></a> </div>
                                            <div id="collapseThree" class="accordion-body collapse">
                                                <div class="accordion-inner">
                                                    <div class="frame">
                                                        <a href="#"><img src="<?=base_url()?>assets/images/about/accordion-img-3.jpg" alt="img"></a>
                                                    </div>
                                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident!</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="about-section-2">
                    <div class="container">
                        <div class="generic-heading">
                            <h2>Our Team</h2>
                            <strong class="title-line">conserve the lands and waters on which all life depends</strong> </div>
                        <div class="row-fluid">
                            <div class="col-md-3">
                                <div class="team-box">
                                    <div class="round">
                                        <a href="#"><img src="<?=base_url()?>assets/images/about/team-img-1.png" alt="img"></a>
                                    </div>
                                    <h3>John Doe</h3>
                                    <strong class="title">CEO / Founder</strong>
                                    <p>Ut elementum adipiscing elit quis viverra. Quisque eu ipsum in justo consectetur consequat. Proin malesuada lacus eget arcu dignissim interdum.</p>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="team-box">
                                    <div class="round">
                                        <a href="#"><img src="<?=base_url()?>assets/images/about/team-img-2.png" alt="img"></a>
                                    </div>
                                    <h3>John Doe</h3>
                                    <strong class="title">CEO / Founder</strong>
                                    <p>Ut elementum adipiscing elit quis viverra. Quisque eu ipsum in justo consectetur consequat. Proin malesuada lacus eget arcu dignissim interdum.</p>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="team-box">
                                    <div class="round">
                                        <a href="#"><img src="<?=base_url()?>assets/images/about/team-img-3.png" alt="img"></a>
                                    </div>
                                    <h3>John Doe</h3>
                                    <strong class="title">CEO / Founder</strong>
                                    <p>Ut elementum adipiscing elit quis viverra. Quisque eu ipsum in justo consectetur consequat. Proin malesuada lacus eget arcu dignissim interdum.</p>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="team-box">
                                    <div class="round">
                                        <a href="#"><img src="<?=base_url()?>assets/images/about/team-img-4.png" alt="img"></a>
                                    </div>
                                    <h3>John Doe</h3>
                                    <strong class="title">CEO / Founder</strong>
                                    <p>Ut elementum adipiscing elit quis viverra. Quisque eu ipsum in justo consectetur consequat. Proin malesuada lacus eget arcu dignissim interdum.</p>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="about-section-3">
                    <div class="container">
                        <div class="row-fluid">
                            <div class="col-md-6">
                                <div class="frame">
                                    <a href="#"><img src="<?=base_url()?>assets/images/about/help-img-1.jpg" alt="img"></a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="text-box">
                                    <h3>Help Us Help you</h3>
                                    <strong class="title">best theme ever for any kind of nonprofit theme</strong>
                                    <ul>
                                        <li><i class="fa fa-check-square-o"></i>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                        <li><i class="fa fa-check-square-o"></i>Sed sollicitudin odio in ipsum egestas pulvinar.</li>
                                        <li><i class="fa fa-check-square-o"></i>Nunc aliquam nisl ut pretium aliquam.</li>
                                        <li><i class="fa fa-check-square-o"></i>Nulla elementum arcu sed leo consectetur dignissim.</li>
                                    </ul>
                                    <strong class="text">Vestibulum ante ipsum primis!</strong> <a href="#" class="readmore">read more</a> </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="about-section-4">
                    <div class="container">
                        <div class="generic-heading">
                            <h2>Our Recent Top Projects</h2>
                            <strong class="title-line">conserve the lands and waters on which all life depends</strong> </div>
                        <ul id="top-project">
                            <li>
                                <div class="row-fluid">
                                    <div class="col-md-4">
                                        <div class="box">
                                            <div class="frame">
                                                <a href="#"><img src="<?=base_url()?>assets/images/about/top-project-img-1.jpg" alt="img"></a>
                                            </div>
                                            <div class="text-box"> <strong class="title">Project Title #1</strong>
                                                <p>Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="box">
                                            <div class="frame">
                                                <a href="#"><img src="<?=base_url()?>assets/images/about/top-project-img-2.jpg" alt="img"></a>
                                            </div>
                                            <div class="text-box"> <strong class="title">Project Title #1</strong>
                                                <p>Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="box">
                                            <div class="frame">
                                                <a href="#"><img src="<?=base_url()?>assets/images/about/top-project-img-3.jpg" alt="img"></a>
                                            </div>
                                            <div class="text-box"> <strong class="title">Project Title #1</strong>
                                                <p>Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row-fluid">
                                    <div class="col-md-4">
                                        <div class="box">
                                            <div class="frame">
                                                <a href="#"><img src="<?=base_url()?>assets/images/about/top-project-img-4.jpg" alt="img"></a>
                                            </div>
                                            <div class="text-box"> <strong class="title">Project Title #1</strong>
                                                <p>Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="box">
                                            <div class="frame">
                                                <a href="#"><img src="<?=base_url()?>assets/images/about/top-project-img-1.jpg" alt="img"></a>
                                            </div>
                                            <div class="text-box"> <strong class="title">Project Title #1</strong>
                                                <p>Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="box">
                                            <div class="frame">
                                                <a href="#"><img src="<?=base_url()?>assets/images/about/top-project-img-3.jpg" alt="img"></a>
                                            </div>
                                            <div class="text-box"> <strong class="title">Project Title #1</strong>
                                                <p>Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row-fluid">
                                    <div class="col-md-4">
                                        <div class="box">
                                            <div class="frame">
                                                <a href="#"><img src="<?=base_url()?>assets/images/about/top-project-img-1.jpg" alt="img"></a>
                                            </div>
                                            <div class="text-box"> <strong class="title">Project Title #1</strong>
                                                <p>Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="box">
                                            <div class="frame">
                                                <a href="#"><img src="<?=base_url()?>assets/images/about/top-project-img-3.jpg" alt="img"></a>
                                            </div>
                                            <div class="text-box"> <strong class="title">Project Title #1</strong>
                                                <p>Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="box">
                                            <div class="frame">
                                                <a href="#"><img src="<?=base_url()?>assets/images/about/top-project-img-4.jpg" alt="img"></a>
                                            </div>
                                            <div class="text-box"> <strong class="title">Project Title #1</strong>
                                                <p>Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </section>
                
            </section>

        </div>
