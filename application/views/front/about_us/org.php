    
    <!--Team Section-->
    <section class="team-section">
        <div class="auto-container">
            <!--Section Title-->
            <div class="sec-title">
                <h2>Expert <strong>Team</strong></h2>
            </div>
            
            <!--Desc Text-->
            <div class="desc-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</div>
            
            
            <div class="row clearfix">
      
                <!--Team Member-->
                <div class="team-member col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="200ms" data-wow-duration="1500ms">
                        <div class="over-info">
                            <h4>Hasib Sharif</h4>
                            <div class="desgnation">CEO</div>
                        </div>
                        <figure class="image-box"><img src="<?=base_url()?>assets/images/resource/team-image-5.jpg" alt=""></figure>
                        <div class="overlay-content">
                            <div class="info-header">
                                <h4>Hasib Sharif</h4>
                                <div class="desgnation">CEO</div>
                            </div>
                            <div class="text">It is a long established fact that a reader will be distracted.</div>
                            <div class="forum">Themexriver</div>
                            <div class="default-social-links">
                                <a href="#"><span class="fa fa-facebook-f"></span></a>
                                <a href="#"><span class="fa fa-twitter"></span></a>
                                <a href="#"><span class="fa fa-google-plus"></span></a>
                                <a href="#"><span class="fa fa-linkedin"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Team Member-->
                <div class="team-member col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="400ms" data-wow-duration="1500ms">
                        <div class="over-info">
                            <h4>Jonathan Breeze</h4>
                            <div class="desgnation">Finance Expert</div>
                        </div>
                        <figure class="image-box"><img src="<?=base_url()?>assets/images/resource/team-image-6.jpg" alt=""></figure>
                        <div class="overlay-content">
                            <div class="info-header">
                                <h4>Jonathan Breeze</h4>
                                <div class="desgnation">Finance Expert</div>
                            </div>
                            <div class="text">It is a long established fact that a reader will be distracted.</div>
                            <div class="forum">Themexriver</div>
                            <div class="default-social-links">
                                <a href="#"><span class="fa fa-facebook-f"></span></a>
                                <a href="#"><span class="fa fa-twitter"></span></a>
                                <a href="#"><span class="fa fa-google-plus"></span></a>
                                <a href="#"><span class="fa fa-linkedin"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Team Member-->
                <div class="team-member col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="over-info">
                            <h4>Tahira Tuz Zohra</h4>
                            <div class="desgnation">Web Designer</div>
                        </div>
                        <figure class="image-box"><img src="<?=base_url()?>assets/images/resource/team-image-7.jpg" alt=""></figure>
                        <div class="overlay-content">
                            <div class="info-header">
                                <h4>Tahira Tuz Zohra</h4>
                                <div class="desgnation">Web Designer</div>
                            </div>
                            <div class="text">It is a long established fact that a reader will be distracted.</div>
                            <div class="forum">Themexriver</div>
                            <div class="default-social-links">
                                <a href="#"><span class="fa fa-facebook-f"></span></a>
                                <a href="#"><span class="fa fa-twitter"></span></a>
                                <a href="#"><span class="fa fa-google-plus"></span></a>
                                <a href="#"><span class="fa fa-linkedin"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
     
                
            </div>
        </div>
    </section>
    