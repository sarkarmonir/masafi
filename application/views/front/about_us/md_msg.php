        <div id="main">
            <section class="about-section">
                <section class="about-section-1">
                    <div class="container">
                        <div class="row-fluid">
                            <div class="">

                            <div class="sec-title">
                                <h2>Managing Director</h2>
                            </div>
                                <div class="about-box-1">
                                    <div class="col-md-3">
                                        <div class="frame">
                                            <a href="#"><img class="img img-responsive" src="<?=base_url()?>assets/images/msg/md.jpg" alt="img"></a>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="text-box-1"> 
                                            <strong class="title"  style="font-size: 22px; text-align: justify">Welcome massage from our Honorable Md </strong><br><br>

                                            <p style="text-align: justify">To our esteemed friends, patrons and business associates, 

                                                Masafi Group has been in the business of Yarn Spinning and Agro based food processing

                                                since last more than 20 years. Now a days, we have made ourselves very much reputed and

                                                prominent organization on Yarn Spinning and Agro based industry in Bangladesh. <br><br>

                                                With happiness to inform you that we have a lot of dynamic and dedicated management

                                                team members and executives, moreover they are always working hard to make the place

                                                in Yarn Spinning and Agro based food processing arena.<br><br>


                                                We are dedicated to fulfill our vision to set the business goal with the assist of core values

                                                by getting our mission assignment. Our dedicated marketing team is very much committed

                                                to increase the market share in Yarn and FMCG market. We offer our clients the most

                                                comprehensive service possible. “Vita Rice” is innovative brand name in Bangladesh; Masafi

                                                Agro food Industries produce processed rice with an excellent combination of Vitamins and

                                                Minerals for the local people who are not getting proper Vitamins and Minerals for their

                                                standard growth.<br><br>


                                                We are not looking only for business, our vision is to get business with the help of human

                                                and humanity. By developing perspective of potential markets prior to initiation we are

                                                able to capitalize on shareholder’s investment.<br><br>


                                                We believe by focusing on customer satisfaction, we ensure the best quality and with the

                                                support of our customers we will go a long way in this business sector.</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>


                </section>

            </div>
