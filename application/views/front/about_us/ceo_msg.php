<div id="main">
    <section class="about-section">
        <section class="about-section-1">
            <div class="container">
                <div class="row-fluid">
                    <div class="">
                        <div class="sec-title">
                            <h2>CEO Director</h2>
                        </div>
                        <div class="about-box-1">
                            <div class="col-md-3">
                                <div class="frame">
                                    <a href="#"><img class="img img-responsive" src="<?=base_url()?>assets/images/msg/chairman.jpg" alt="img"></a>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="text-box-1"> 
                                    <strong class="title">Welcome massage from our Honorable Chairman </strong>
                                    <p>
                                        First thanks to Almighty ALLAH, giving me the patience and endurance to go ahead. It is always been praiseworthy telling about 
                                        “MASAFI GROUP’’ spreading into many branches like a fruitful tree that offers enormous shadows to the different walks of people.<br><br>
                                        It is very important at the beginning of a business to set out a vision for the future and my vision was already set - founding 
                                        a group of companies with diversified products and services, creating opportunities of employment, turning the population into 
                                        power, simplify the self dependency of the country. Although MASAFI GROUP of Industries has reached a certain level of achievements, 
                                        the vision keeps telling me opportunities are unlimited and there is much more to be done.<br><br>
                                        Our vision, mission and values altogether made our trying remarkable that headed the company years together bathed with the customer’s 
                                        spontaneous support within a short col-md- of time.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</div>
