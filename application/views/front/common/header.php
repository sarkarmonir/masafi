<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Masafi Group</title>
    <!-- Stylesheets -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/revolution-slider.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">

    <!--Color Switcher Style-->
    <link href="<?php echo base_url(); ?>assets/css/color-schemes/colors.css" rel="stylesheet">
    <!--Color Switcher Style-->

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/x-icon">
    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">



    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="<?php echo base_url(); ?>assets/js/respond.js"></script><![endif]-->
</head>

<body>

    <div class="page-wrapper">


        <!-- Preloader -->
        <!-- <div class="preloader"></div> -->

        <!-- Main Header-->
        <header class="main-header">
            <!-- Header Top -->
            <div class="header-top">
                <div class="auto-container">
                    <div class="row clearfix">

                        <!--Top Left-->
                        <div class="top-left col-md-6 col-sm-6 col-xs-12 pull-left">
                            <ul>
                                <li><a href="#"><span class="fa fa-phone"></span> +88 01717 231 363</a></li>
                                <li><a href="mailto:info@masafigroup.com"><span class="fa fa-envelope"></span> info@masafigroup.com</a></li>
                            </ul>
                        </div>

                        <!--Top Right-->
                        <div class="top-right col-md-6 col-sm-6 col-xs-12 pull-left">
                            <ul>
                                <li><a href="#">ENG</a></li>
                                <li><a class="share" href="#"><span class="fa fa-share-alt"></span></a></li>
                            </ul>
                        </div>

                    </div>

                </div>
            </div><!-- Header Top End -->


            <!-- Main Box -->
            <div class="main-box">
                <div class="auto-container">
                    <div class="outer-container clearfix">
                        <!--Logo Box-->
                        <div class="logo-box">
                            <div class="logo"><a href="<?=site_url()?>"><img src="<?=site_url()?>assets/images/logo.png" alt=""></a></div>
                        </div>

                        <!--Nav Outer-->
                        <div class="nav-outer clearfix">
                            <!-- Main Menu -->
                            <nav class="main-menu">
                                <div class="navbar-header">
                                    <!-- Toggle Button -->      
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="navbar-collapse collapse clearfix">
                                    <ul class="navigation clearfix">
                                        <li class="current"><a href="<?=site_url()?>">Home</a></li>
                                        <li class="dropdown"><a href="#">About us</a>
                                            <ul>
                                                <li><a href="<?=base_url()?>sister_concern/details/shirin_spinning_mills_ltd" >About Masafi Group</a></li>
                                                <li><a href="<?=site_url("about_us/message_from_chairman")?>">Message from chairman</a></li>
                                                <li><a href="<?=site_url("about_us/message_from_md")?>">Message from MD</a></li>
                                                <li><a href="<?=site_url("about_us/message_from_ceo")?>">Message from CEO</a></li>
                                                <li><a href="<?=site_url("about_us/leadership")?>">Leadership</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown"><a href="#">Sister Concern</a>
                                            <ul>
                                                <?php foreach($this->concern->menu as $menu) : ?>
                                                    <li><a href="<?=base_url()?>sister_concern/details/<?=$menu->url?>" ><?=$menu->name?></a></li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </li>
                                        <li><a href="<?=site_url("gallery")?>">Gallery</a></li>
                                        <li><a href="<?=site_url("news_events")?>">News & Events</a></li>
                                        <li><a href="<?=site_url("under_construction")?>">Awards & Accredition</a></li>
                                        <li><a href="<?=site_url("csr")?>">CSR</a></li>
                                        <li><a href="<?=site_url("under_construction")?>">Career</a></li>
                                        <li><a href="<?=site_url("contact_us")?>">Contact Us</a></li>
                                    </ul>
                                </div>
                            </nav><!-- Main Menu End-->

                        </div><!--Nav Outer End-->

                        <!-- Hidden Nav Toggler -->
                        <div class="nav-toggler">
                            <button class="hidden-bar-opener"><span class="icon fa fa-bars"></span></button>
                        </div><!-- / Hidden Nav Toggler -->


                    </div>    
                </div>
            </div>

        </header>
        <!--End Main Header -->


        <!-- Hidden Navigation Bar -->
        <section class="hidden-bar right-align">

            <div class="hidden-bar-closer">
                <button class="btn"><i class="fa fa-close"></i></button>
            </div>

            <!-- Hidden Bar Wrapper -->
            <div class="hidden-bar-wrapper">

                <!-- .logo -->
                <div class="logo text-center">
                    <a href="<?=site_url()?>"><img src="<?=site_url()?>assets/images/logo.png" alt=""></a>         
                </div><!-- /.logo -->

                <!-- .Side-menu -->
                <div class="side-menu">
                    <!-- .navigation -->
                    <ul class="navigation clearfix">
                        <li class="current"><a href="<?=site_url()?>">Home</a></li>
                        <li class="dropdown"><a href="#">About us</a>
                            <ul>
                                <li><a href="<?=site_url()?>home/chairman_msg">About Masafi Group</a></li>
                                <li><a href="<?=site_url()?>home/chairman_msg">Message from chairman</a></li>
                                <li><a href="<?=site_url()?>home/md_msg">Message from MD</a></li>
                                <li><a href="<?=site_url()?>home/ceo_msg">Message from CEO</a></li>
                                <li><a href="<?=site_url()?>home/leadership">Leadership</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#">Sister Concern</a>
                            <ul>
                                <li><a href="<?=site_url()?>home/shirin_spinning_mills_ltd">Shirin Spinning Mills Ltd.</a></li>
                                <li><a href="<?=site_url()?>home/diamond">Masafi Bread & Biscuit Ind. Ltd.</a></li>
                                <li><a href="<?=site_url()?>home/abr_spinning_mills_ltd">AB.R-Spinning Mills Ltd.</a></li>
                                <li><a href="<?=site_url()?>home/apon_food_products_ltd">Apon Food Products Ltd.</a></li>
                                <li><a href="<?=site_url()?>home/diamond">Masafi Flour Mills Ltd.</a></li>
                            </ul>
                        </li>
                        <li><a href="<?=site_url()?>home/gallery">Gallery</a></li>
                        <li><a href="<?=site_url()?>home/blog">News & Events</a></li>
                        <li><a href="<?=site_url()?>home/under_construction">Awards & Accredition</a></li>
                        <li><a href="<?=site_url()?>home/csr">CSR</a></li>
                        <li><a href="<?=site_url()?>home/under_construction">Career</a></li>
                        <li><a href="<?=site_url()?>home/contact">Contact Us</a></li>
                    </ul>
                </div><!-- /.Side-menu -->

                <div class="social-icons">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>

            </div><!-- / Hidden Bar Wrapper -->
    </section><!-- / Hidden Bar -->