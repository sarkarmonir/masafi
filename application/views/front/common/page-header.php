    <section class="page-title" style="background-image:url(<?=base_url($img)?>);">
    	<div class="auto-container">
        	<h1><?=$title?></h1>
            <ul class="bread-crumb">
                <li><a href="index-2.html">About us</a></li>
                <li class="active"><?=$title?></li>
            </ul>
        </div>
    </section>