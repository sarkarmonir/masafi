    <!--Featured Projects-->
    <section class="featured-projects">
        <div class="auto-container">
            <!--Section Title-->
            <div class="sec-title">
                <h2>Sister <strong>Concern</strong></h2>
            </div>
            
            <div class="carousel-outer">
                <!--Featured Projects Carousel-->
                <div class="featured-project-carousel">
                    <!--SLide Item-->
                    <div class="featured-project-column">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="<?=base_url()?>assets/images/sis/1.jpg" alt=""></a><a href="#" class="theme-btn link-btn">View Project</a></figure>
                            
                        </div>
                    </div>
                    <!--SLide Item-->
                    <div class="featured-project-column">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="<?=base_url()?>assets/images/sis/2.jpg" alt=""></a><a href="#" class="theme-btn link-btn">View Project</a></figure>
                            
                        </div>
                    </div>
                    <!--SLide Item-->
                    <div class="featured-project-column">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="<?=base_url()?>assets/images/sis/3.jpg" alt=""></a><a href="#" class="theme-btn link-btn">View Project</a></figure>
                            
                        </div>
                    </div>
                    <!--SLide Item-->
                    <div class="featured-project-column">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="<?=base_url()?>assets/images/sis/4.png" alt=""></a><a href="#" class="theme-btn link-btn">View Project</a></figure>
                           
                        </div>
                    </div>
                    <!--SLide Item-->
                    <div class="featured-project-column">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="<?=base_url()?>assets/images/sis/1.jpg" alt=""></a><a href="#" class="theme-btn link-btn">View Project</a></figure>
                            
                        </div>
                    </div>
                    <!--SLide Item-->
                    <div class="featured-project-column">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="<?=base_url()?>assets/front/images/sis/2.jpg" alt=""></a><a href="#" class="theme-btn link-btn">View Project</a></figure>
                            
                        </div>
                    </div>
                   
                </div>
            </div>
            
        </div>
    </section>
    <!--Main Footer-->
    <footer class="main-footer">
        
        <!--Widgets Section-->
        <div class="widgets-section" style="background-image:url(<?=base_url()?>assets/images/footer.png);">
            <div class="auto-container">
                <div class="row clearfix">
                    <!--Big Column-->
                    <div class="big-column col-md-6 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                            
                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget about-widget">
                                    <h2>Masafi Group</h2>
                                    <div class="widget-content">
                                        <div class="text">
                                         Masafi Group has been established in 2000 as a Trading company as well as
                                         manufacturing company of Yarn Spinning and Agro based food manufacturing 
                                         industry. 
                                         </div>
                                        <div class="social-links">
                                            <a href="#"><span class="fa fa-facebook-f"></span></a>
                                            <a href="#"><span class="fa fa-twitter"></span></a>
                                            <a href="#"><span class="fa fa-google-plus"></span></a>
                                            <a href="#"><span class="fa fa-linkedin"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget links-widget">
                                    <h2>Quick Links</h2>
                                    <div class="widget-content">
                                        <ul class="list">
                                            <li><a href="#">Home</a></li>
                                            <li><a href="#">About Us</a></li>
                                            <li><a href="#">Services</a></li>
                                            <li><a href="#">News</a></li>
                                            <li><a href="#">Contact Us</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
                    
                    <!--Big Column-->
                    <div class="big-column col-md-6 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                            
                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget posts-widget">
                                    <h2>Recent News</h2>
                                    <div class="widget-content">
                                        <!--Post-->
                                        <div class="post">
                                            <div class="content">
                                                <figure class="post-thumb"><a href="#"><img src="<?=base_url()?>assets/images/resource/post-thumb-1.jpg" alt=""></a></figure>
                                                <h4><a href="#">Masafi Group news 1</a></h4>
                                                <div class="time">5 hours ago</div>
                                            </div>
                                        </div>
                                        <!--Post-->
                                        <div class="post">
                                            <div class="content">
                                                <figure class="post-thumb"><a href="#"><img src="<?=base_url()?>assets/images/resource/post-thumb-2.jpg" alt=""></a></figure>
                                                <h4><a href="#">Masafi Group news 2</a></h4>
                                                <div class="time">5 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget contact-widget">
                                    <h2>Contact Info</h2>
                                    <div class="widget-content">
                                        <ul class="contact-info">
                                            <li><span class="icon fa fa-map-marker"></span> <strong>Gulshan, </strong>Jackson Dhaka-1212, Bangladesh</li>
                                            <li><span class="icon fa fa-mobile"></span> <strong>+88 01717 231 363</strong>masafi@masafigroup.com</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                 </div>
             </div>
         </div>
         
         <!--Footer Bottom-->
         <div class="footer-bottom">
            <div class="auto-container">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12"><div class="copyright">Copyright &copy; <span class="year">2017</span>. All rights are reserved.</div></div>
                    <div class="col-md-6 col-sm-12"><div class="developed-by">Made with   by <a target="_blank" href="#" class="author-name">CloudNextLTD</a></div></div>
                </div>
            </div>
         </div>
         
    </footer>
    
    
</div>
<!--End pagewrapper-->


<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="fa fa-long-arrow-up"></span></div>


<script src="<?=base_url()?>assets/js/jquery.js"></script> 
<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?=base_url()?>assets/js/revolution.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.fancybox.pack.js"></script>
<script src="<?=base_url()?>assets/js/jquery.fancybox-media.js"></script>
<script src="<?=base_url()?>assets/js/owl.js"></script>
<script src="<?=base_url()?>assets/js/wow.js"></script>
<script src="<?=base_url()?>assets/js/color-settings.js"></script>
<script src="<?=base_url()?>assets/js/script.js"></script>

</body>
</html>