    <!--Sidebar Page-->
    <div class="sidebar-page-container">
        <div class="container">
            <div class="row clearfix">
                
                
                <!--Content Side--> 
                <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    
                    <!--Blog News Section-->
                    <section class="news-section blog-detail no-padd-bottom no-padd-top">
                        
                        <!--Default News Post-->
                        <div class="default-news-post">
                            <div class="inner-box">
                                <figure class="image-box"><img src="<?=base_url()?>assets/images/resource/blog-image-10.jpg" alt=""></figure>
                                <div class="lower-content">
                                    <h3>Growth of finance around the third world.</h3>
                                    <ul class="post-meta clearfix">
                                        <li><a href="#">February 19</a></li>
                                        <li><a href="#">Hasib Sharif</a></li>
                                        <li><a href="#">Gardening</a></li> 
                                        <li><a href="#"><span class="icon flaticon-multimedia-7"></span> 15</a></li>
                                    </ul>
                                    <div class="text">
                                        <p>A wild misconception of social media experts is that we spend most of our time sitting around watching YouTube videos. However, my friends, I’m sorry to .break this to you, you’ve been fooled! In a way, we’re more like experience designers. If you take a look at some creative agency structures the roles are pretty defined; you’ve got your strategists, designers, creative teams etc.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper.</p>
                                        <blockquote>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla dolore eu feugiat nulla facilisis at vero eros demonstraverunt.</blockquote>
                                        <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram.</p>
                                    </div>
                                    
                                    <div class="social-links text-right">
                                        <strong>Share This Post</strong> 
                                        <a href="#" class="facebook img-circle"><span class="fa fa-facebook"></span></a>
                                        <a href="#" class="twitter img-circle"><span class="fa fa-twitter"></span></a>
                                        <a href="#" class="google-plus img-circle"><span class="fa fa-google-plus"></span></a>
                                        <a href="#" class="linkedin img-circle"><span class="fa fa-linkedin"></span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                    </section>

                </div>
                <!--Content Side-->
                
                <!--Sidebar-->  
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <aside class="sidebar">
                        
                        
                        <!-- Recent Articles -->
                        <div class="sidebar-widget boxed-widget links-widget wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="widget-title"><h2>CATEGORY</h2></div>
                            
                            <ul class="list">
                                <li><a href="#"><div class="clearfix"><span class="pull-left">Food</span> <span class="pull-right">(10)</span></div></a></li>
                                <li><a href="#"><div class="clearfix"><span class="pull-left">Water</span> <span class="pull-right">(08)</span></div></a></li>
                                <li class="current"><a href="#"><div class="clearfix"><span class="pull-left">Education</span> <span class="pull-right">(02)</span></div></a></li>
                                <li><a href="#"><div class="clearfix"><span class="pull-left">Africa</span> <span class="pull-right">(03)</span></div></a></li>
                                <li><a href="#"><div class="clearfix"><span class="pull-left">India</span> <span class="pull-right">(08)</span></div></a></li>
                                <li><a href="#"><div class="clearfix"><span class="pull-left">Event</span> <span class="pull-right">(08)</span></div></a></li>
                            </ul>
                            
                        </div>
                        
                        
                        <!-- Popular Posts -->
                        <div class="sidebar-widget boxed-widget popular-posts wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="widget-title"><h2>RECENT POST</h2></div>
                            
                            <article class="post">
                                <figure class="post-thumb"><a href="#"><img src="<?=base_url()?>assets/images/resource/post-thumb-3.jpg" alt=""></a></figure>
                                <h4><a href="#">Micenas Placerat Nibh Loreming</a></h4>
                                <div class="post-info">SEP 29, 2015</div>
                            </article>
                            
                            <article class="post">
                                <figure class="post-thumb"><a href="#"><img src="<?=base_url()?>assets/images/resource/post-thumb-4.jpg" alt=""></a></figure>
                                <h4><a href="#">Integer Suscipit Sit Amet</a></h4>
                                <div class="post-info">SEP 29, 2015</div>
                            </article>
                            
                            <article class="post">
                                <figure class="post-thumb"><a href="#"><img src="<?=base_url()?>assets/images/resource/post-thumb-5.jpg" alt=""></a></figure>
                                <h4><a href="#">Praeent Vehicula Neget Tempus</a></h4>
                                <div class="post-info">SEP 29, 2015</div>
                            </article>
                            
                        </div>
                        
                        
                        
                                
                    </aside>
                
                    
                </div>
                <!--Sidebar-->  
                
                      
                
            </div>
        </div>
    </div>