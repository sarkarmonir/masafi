
<section class="about-section">
  <div class="container">
    <div class="row-fluid">
      
      <div class="col-md-3">
        <div class="about-box-1">
          <div class="sec-title left-aligned">
            <h2>Masafi's CSR</h2>
          </div>
          <nav class="nav-sidebar">
            <ul class="nav tabs">
              <li class="active"><a href="#tab1" data-toggle="tab">Overview</a></li>
              <li class=""><a href="#tab2" data-toggle="tab">Education</a></li>
              <li class=""><a href="#tab3" data-toggle="tab">Health care services</a></li>
              <li class=""><a href="#tab4" data-toggle="tab">Environmental Programs</a></li>
              <li class=""><a href="#tab5" data-toggle="tab">Religious Donation</a></li> 							
              <li class=""><a href="#tab6" data-toggle="tab">Relief Funds</a></li>                            
            </ul>
          </nav>
        </div>
      </div>
      <div class="col-md-9">
        <div class="">
          <!-- tab content -->
          <div class="tab-content">
            <div class="tab-pane active text-style" id="tab1">
              <div class="sec-title left-aligned ">
                <h2>Overview</h2>
              </div> 
              <div class="about-box-1">
                <p>To manufacture products comparable to international standards, to be customer-focused and globally competitive through better quality, latest technology and continuous innovation.<br><br>
                  To be recognized by our customers as the best strategic partner in our product that fully participate a meaningful role on sustainable development in national economy.</p>
                  <hr>
                  <iframe width="100%" height="350" src="https://www.youtube.com/embed/MzLEyED-xUs" frameborder="0" allowfullscreen></iframe>
                  
                </div>
              </div>
              <div class="tab-pane text-style" id="tab2">
                <div class="sec-title left-aligned">
                  <h2>Education</h2>
                </div>
                <div class="about-box-1">
                  <p style="text-align: justify">Our Companies’ Core Values of Leadership, Integrity, Respect, Innovation and Continuous Improvement are at the heart of everything we do. These Core Values are a source of inspiration as we lead the way to a brighter future for our company and all who depend on it.  They support Masafi Groups' purpose: Making lives better one call at a time, by connecting people to solutions.
                    <br><br><strong>Leadership   </strong><br>
                    <col-md- style="color: blue">We develop leaders... </col-md-> <br>

                    We foster trust and collaboration to develop leaders focused on sustainable, superior performance.  We set positive examples and invest in others so that they can follow the guidelines of leadership.
                    <br>
                    <br>

                  </p>
                  
                </div>
              </div>
              <div class="tab-pane text-style" id="tab3">
                <div class="sec-title left-aligned">
                  <h2>Health Care Services</h2>
                </div>
                <div class="about-box-1">
                  <p style="text-align: justify">A major component of sustainable development of a nation is improving standard of living and maintaining it. Healthcare in Bangladesh a major obstacle when it comes to achieving the benchmarked living standards for a developing country. Healthcare for a majority of the people is often considered a privilege rather than what it should be, a birth right. Keeping this in mind, MASAFI Group has taken initiatives in the past and continues to do so to provide healthcare services through its various projects to the needy. Some of the more mentionable projects are as follows:
                  </p>
                  
                </div>
              </div>



              <div class="tab-pane text-style" id="tab4">
                <div class="sec-title left-aligned">
                  <h2 class="title-opt">Environmental Programs</h2></div>
                  <div class="about-box-1">
                    <p style="text-align: justify">Global warming is truly an “inconvenient truth” (as termed by Al Gore) that all the members of planet have to live with. To slow down the process of global warming there needs to be more public awareness. Through varities projects. MASAFI Group plays its part in developing the mindset of the youth in such a way that they are aware of the latest environmental issues that concern everyone on a global level.

                    </p>
                    
                  </div>
                </div>

                <div class="tab-pane text-style" id="tab5">
                  <div class="sec-title left-aligned">
                    <h2 class="title-opt">Religious Donation</h2></div>
                    <div class="about-box-1">
                      <p style="text-align: justify">Our Companies’ Core Values of Leadership, Integrity, Respect, Innovation and Continuous Improvement are at the heart of everything we do. These Core Values are a source of inspiration as we lead the way to a brighter future for our company and all who depend on it.  They support Masafi Groups' purpose: Making lives better one call at a time, by connecting people to solutions.
                        <br><br><strong>Leadership   </strong><br>
                        <col-md- style="color: blue">We develop leaders... </col-md-> <br>

                        We foster trust and collaboration to develop leaders focused on sustainable, superior performance.  We set positive examples and invest in others so that they can follow the guidelines of leadership.
                        <br>
                        <br>

                      </p>
                      
                    </div>
                  </div>
                  
                  <div class="tab-pane text-style" id="tab6">
                    <div class="sec-title left-aligned">
                      <h2 class="title-opt">Relief Funds</h2></div>
                      <div class="about-box-1">
                        <p style="text-align: justify">MASAFI Group has always been involved in contributing to relief funds whether they be administered by the government or privately by charitable organizations. The proceeds from the funds go to disadvantaged people in backward regions of the country affected by natural calamities, orphans and disadvantaged females and financially disadvantaged talented young students with great potential to do well in academics.

                        </p>
                        
                      </div>
                    </div>
                    
                    
                  </div>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </section>
