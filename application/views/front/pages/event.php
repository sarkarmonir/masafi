
        <div id="main">
            <section class="generic-heading-3">
                <div class="container">
                    <h1>Our Events</h1>
                    <strong class="title-line">conserve the lands and waters on which all life depends</strong> </div>
            </section>
            <section class="blog-page">
                <div class="container">
                    <div class="row-fluid">
                        <div class="col-md-8">
                            <div class="event-box">
                                <ul class="gallery">
                                    <li>
                                        <div class="top">
                                            <div class="frame"> <img src="<?php echo base_url(); ?>assets/front/images/event/event-img-1.jpg" alt="img"> <a href="<?php echo base_url(); ?>assets/front/images/event/event-img-1.jpg" data-rel="prettyPhoto[gallery1]"><i class="fa fa-search-plus"></i></a> </div>
                                            <div class="text-box">
                                                <h2>An Evening With Paul Farmer</h2>
                                                <strong class="title">Liporttitor mauris sit amet orean dignis</strong>
                                                <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus dignisiim equie lipor titor alseuqm.</p>
                                                <a href="<?php echo base_url(); ?>home/event_details">more<i class="fa fa-plus-circle"></i></a> </div>
                                        </div>
                                        <div class="bottom">
                                            <div class="time-area"> <strong class="time">9:00am to 11:00am</strong> <strong class="date">23 March, 2014</strong> </div>
                                            <div class="event-time-box">
                                                <div class="defaultCountdown-2"></div>
                                            </div>
                                            <a href="#" class="btn-register">REGISTER</a> </div>
                                    </li>
                                    <li>
                                        <div class="top">
                                            <div class="frame"> <img src="<?php echo base_url(); ?>assets/front/images/event/event-img-2.jpg" alt="img"> <a href="<?php echo base_url(); ?>assets/front/images/event/event-img-2.jpg" data-rel="prettyPhoto[gallery1]"><i class="fa fa-search-plus"></i></a> </div>
                                            <div class="text-box">
                                                <h2>An Evening With Paul Farmer</h2>
                                                <strong class="title">Liporttitor mauris sit amet orean dignis</strong>
                                                <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus dignisiim equie lipor titor alseuqm.</p>
                                                <a href="<?php echo base_url(); ?>home/event_details">more<i class="fa fa-plus-circle"></i></a> </div>
                                        </div>
                                        <div class="bottom">
                                            <div class="time-area"> <strong class="time">9:00am to 11:00am</strong> <strong class="date">23 March, 2014</strong> </div>
                                            <div class="event-time-box">
                                                <div class="defaultCountdown-2"></div>
                                            </div>
                                            <a href="#" class="btn-register">REGISTER</a> </div>
                                    </li>
                                    <li>
                                        <div class="top">
                                            <div class="frame"> <img src="<?php echo base_url(); ?>assets/front/images/event/event-img-3.jpg" alt="img"> <a href="<?php echo base_url(); ?>assets/front/images/event/event-img-3.jpg" data-rel="prettyPhoto[gallery1]"><i class="fa fa-search-plus"></i></a> </div>
                                            <div class="text-box">
                                                <h2>An Evening With Paul Farmer</h2>
                                                <strong class="title">Liporttitor mauris sit amet orean dignis</strong>
                                                <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus dignisiim equie lipor titor alseuqm.</p>
                                                <a href="<?php echo base_url(); ?>home/event_details">more<i class="fa fa-plus-circle"></i></a> </div>
                                        </div>
                                        <div class="bottom">
                                            <div class="time-area"> <strong class="time">9:00am to 11:00am</strong> <strong class="date">23 March, 2014</strong> </div>
                                            <div class="event-time-box">
                                                <div class="defaultCountdown-2"></div>
                                            </div>
                                            <a href="#" class="btn-register">REGISTER</a> </div>
                                    </li>
                                    <li>
                                        <div class="top">
                                            <div class="frame"> <img src="<?php echo base_url(); ?>assets/front/images/event/event-img-4.jpg" alt="img"> <a href="<?php echo base_url(); ?>assets/front/images/event/event-img-4.jpg" data-rel="prettyPhoto[gallery1]"><i class="fa fa-search-plus"></i></a> </div>
                                            <div class="text-box">
                                                <h2>An Evening With Paul Farmer</h2>
                                                <strong class="title">Liporttitor mauris sit amet orean dignis</strong>
                                                <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus dignisiim equie lipor titor alseuqm.</p>
                                                <a href="<?php echo base_url(); ?>home/event_details">more<i class="fa fa-plus-circle"></i></a> </div>
                                        </div>
                                        <div class="bottom">
                                            <div class="time-area"> <strong class="time">9:00am to 11:00am</strong> <strong class="date">23 March, 2014</strong> </div>
                                            <div class="event-time-box">
                                                <div class="defaultCountdown-2"></div>
                                            </div>
                                            <a href="#" class="btn-register">REGISTER</a> </div>
                                    </li>
                                    <li>
                                        <div class="top">
                                            <div class="frame"> <img src="<?php echo base_url(); ?>assets/front/images/event/event-img-5.jpg" alt="img"> <a href="<?php echo base_url(); ?>assets/front/images/event/event-img-5.jpg" data-rel="prettyPhoto[gallery1]"><i class="fa fa-search-plus"></i></a> </div>
                                            <div class="text-box">
                                                <h2>An Evening With Paul Farmer</h2>
                                                <strong class="title">Liporttitor mauris sit amet orean dignis</strong>
                                                <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus dignisiim equie lipor titor alseuqm.</p>
                                                <a href="<?php echo base_url(); ?>home/event_details">more<i class="fa fa-plus-circle"></i></a> </div>
                                        </div>
                                        <div class="bottom">
                                            <div class="time-area"> <strong class="time">9:00am to 11:00am</strong> <strong class="date">23 March, 2014</strong> </div>
                                            <div class="event-time-box">
                                                <div class="defaultCountdown-2"></div>
                                            </div>
                                            <a href="#" class="btn-register">REGISTER</a> </div>
                                    </li>
                                </ul>

                                <div class="pagination-area">
                                    <div class="pagination">
                                        <ul>
                                            <li class="active"><a href="#">01</a></li>
                                            <li><a href="#">02</a></li>
                                            <li><a href="#">03</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <aside class="sidebar">
                                <div class="sidebar-tab">
                                    <div class="">
                                        <div class="" id="tab-1">
                                            <div class="sidebar-tab-content">
                                            <h2>Article</h2>
                                                <ul>
                                                    <li>
                                                        <div class="thumb">
                                                            <a href="#"><img src="<?php echo base_url(); ?>assets/front/images/blog/sidebar-tab-thumb-img-1.jpg" alt="img"></a>
                                                        </div>
                                                        <div class="text-box">
                                                            <p>Lorem Ipsum harums ser quidem sit rerums facilis desk team</p>
                                                            <a href="#" class="mnt">November 01, 2013,</a> <a href="#" class="comment">9 Comments</a> </div>
                                                    </li>
                                                    <li>
                                                        <div class="thumb">
                                                            <a href="#"><img src="<?php echo base_url(); ?>assets/front/images/blog/sidebar-tab-thumb-img-2.jpg" alt="img"></a>
                                                        </div>
                                                        <div class="text-box">
                                                            <p>Lorem Ipsum harums ser quidem sit rerums facilis desk team</p>
                                                            <a href="#" class="mnt">November 02, 2013,</a> <a href="#" class="comment">10 Comments</a> </div>
                                                    </li>
                                                    <li>
                                                        <div class="thumb">
                                                            <a href="#"><img src="<?php echo base_url(); ?>assets/front/images/blog/sidebar-tab-thumb-img-3.jpg" alt="img"></a>
                                                        </div>
                                                        <div class="text-box">
                                                            <p>Lorem Ipsum harums ser quidem sit rerums facilis desk team</p>
                                                            <a href="#" class="mnt">November 03, 2013,</a> <a href="#" class="comment">11 Comments</a> </div>
                                                    </li>
                                                    <li>
                                                        <div class="thumb">
                                                            <a href="#"><img src="<?php echo base_url(); ?>assets/front/images/blog/sidebar-tab-thumb-img-3.jpg" alt="img"></a>
                                                        </div>
                                                        <div class="text-box">
                                                            <p>Lorem Ipsum harums ser quidem sit rerums facilis desk team</p>
                                                            <a href="#" class="mnt">November 03, 2013,</a> <a href="#" class="comment">11 Comments</a> </div>
                                                    </li>
                                                    <li>
                                                        <div class="thumb">
                                                            <a href="#"><img src="<?php echo base_url(); ?>assets/front/images/blog/sidebar-tab-thumb-img-3.jpg" alt="img"></a>
                                                        </div>
                                                        <div class="text-box">
                                                            <p>Lorem Ipsum harums ser quidem sit rerums facilis desk team</p>
                                                            <a href="#" class="mnt">November 03, 2013,</a> <a href="#" class="comment">11 Comments</a> </div>
                                                    </li>
                                                    <li>
                                                        <div class="thumb">
                                                            <a href="#"><img src="<?php echo base_url(); ?>assets/front/images/blog/sidebar-tab-thumb-img-3.jpg" alt="img"></a>
                                                        </div>
                                                        <div class="text-box">
                                                            <p>Lorem Ipsum harums ser quidem sit rerums facilis desk team</p>
                                                            <a href="#" class="mnt">November 03, 2013,</a> <a href="#" class="comment">11 Comments</a> </div>
                                                    </li>
                                                    <li>
                                                        <div class="thumb">
                                                            <a href="#"><img src="<?php echo base_url(); ?>assets/front/images/blog/sidebar-tab-thumb-img-3.jpg" alt="img"></a>
                                                        </div>
                                                        <div class="text-box">
                                                            <p>Lorem Ipsum harums ser quidem sit rerums facilis desk team</p>
                                                            <a href="#" class="mnt">November 03, 2013,</a> <a href="#" class="comment">11 Comments</a> </div>
                                                    </li>
                                                </ul>
                                                <a href="#" class="find-more">Find Out More</a> </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </section>
        </div>
