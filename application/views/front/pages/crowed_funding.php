<div id="main">
    <section class="generic-heading-3">
        <div class="container">
            <h1>Receipi</h1>
            <strong class="title-line">conserve the lands and waters on which all life depends</strong> </div>
    </section>

    <section class="gallery-two-column">
        <div class="container">
            <ul class="row gallery">
                <li class="col-md-3">
                    <div class="gallery-box">
                        <div class="frame">
                            <a href="#"><img src="<?php echo base_url(); ?>assets/front/images/gallety-4-column/gallery-4-column-img-1.jpg" alt="img"></a>
                            <div class="caption"><a href="<?php echo base_url(); ?>assets/front/images/gallety-4-column/gallery-4-column-img-1.jpg" data-rel="prettyPhoto[gallery1]"><i class="fa fa-search-plus"></i></a></div>
                        </div>
                        <div class="text-box">
                            <h2>Project Title #1</h2>
                            <p>Etiam porta sem malesuada magnais llis euismod. Aenean lacinia bibendui nec id elit non Aenean lacinia bibendui nec id elit non mi porta donec....</p>
                        </div>
                    </div>
                </li>
                <li class="col-md-3">
                    <div class="gallery-box">
                        <div class="frame">
                            <a href="#"><img src="<?php echo base_url(); ?>assets/front/images/gallety-4-column/gallery-4-column-img-2.jpg" alt="img"></a>
                            <div class="caption"><a href="<?php echo base_url(); ?>assets/front/images/gallety-4-column/gallery-4-column-img-2.jpg" data-rel="prettyPhoto[gallery1]"><i class="fa fa-search-plus"></i></a></div>
                        </div>
                        <div class="text-box">
                            <h2>Project Title #2</h2>
                            <p>Etiam porta sem malesuada magnais llis euismod. Aenean lacinia bibendui nec id elit non Aenean lacinia bibendui nec id elit non mi porta donec....</p>
                        </div>
                    </div>
                </li>
                <li class="col-md-3">
                    <div class="gallery-box">
                        <div class="frame">
                            <a href="#"><img src="<?php echo base_url(); ?>assets/front/images/gallety-4-column/gallery-4-column-img-3.jpg" alt="img"></a>
                            <div class="caption"><a href="<?php echo base_url(); ?>assets/front/images/gallety-4-column/gallery-4-column-img-3.jpg" data-rel="prettyPhoto[gallery1]"><i class="fa fa-search-plus"></i></a></div>
                        </div>
                        <div class="text-box">
                            <h2>Project Title #2</h2>
                            <p>Etiam porta sem malesuada magnais llis euismod. Aenean lacinia bibendui nec id elit non Aenean lacinia bibendui nec id elit non mi porta donec....</p>
                        </div>
                    </div>
                </li>
                <li class="col-md-3">
                    <div class="gallery-box">
                        <div class="frame">
                            <a href="#"><img src="<?php echo base_url(); ?>assets/front/images/gallety-4-column/gallery-4-column-img-4.jpg" alt="img"></a>
                            <div class="caption"><a href="<?php echo base_url(); ?>assets/front/images/gallety-4-column/gallery-4-column-img-4.jpg" data-rel="prettyPhoto[gallery1]"><i class="fa fa-search-plus"></i></a></div>
                        </div>
                        <div class="text-box">
                            <h2>Project Title #2</h2>
                            <p>Etiam porta sem malesuada magnais llis euismod. Aenean lacinia bibendui nec id elit non Aenean lacinia bibendui nec id elit non mi porta donec....</p>
                        </div>
                    </div>
                </li>
                <li class="col-md-3">
                    <div class="gallery-box">
                        <div class="frame">
                            <a href="#"><img src="<?php echo base_url(); ?>assets/front/images/gallety-4-column/gallery-4-column-img-5.jpg" alt="img"></a>
                            <div class="caption"><a href="<?php echo base_url(); ?>assets/front/images/gallety-4-column/gallery-4-column-img-5.jpg" data-rel="prettyPhoto[gallery1]"><i class="fa fa-search-plus"></i></a></div>
                        </div>
                        <div class="text-box">
                            <h2>Project Title #2</h2>
                            <p>Etiam porta sem malesuada magnais llis euismod. Aenean lacinia bibendui nec id elit non Aenean lacinia bibendui nec id elit non mi porta donec....</p>
                        </div>
                    </div>
                </li>
                <li class="col-md-3">
                    <div class="gallery-box">
                        <div class="frame">
                            <a href="#"><img src="<?php echo base_url(); ?>assets/front/images/gallety-4-column/gallery-4-column-img-6.jpg" alt="img"></a>
                            <div class="caption"><a href="<?php echo base_url(); ?>assets/front/images/gallety-4-column/gallery-4-column-img-6.jpg" data-rel="prettyPhoto[gallery1]"><i class="fa fa-search-plus"></i></a></div>
                        </div>
                        <div class="text-box">
                            <h2>Project Title #2</h2>
                            <p>Etiam porta sem malesuada magnais llis euismod. Aenean lacinia bibendui nec id elit non Aenean lacinia bibendui nec id elit non mi porta donec....</p>
                        </div>
                    </div>
                </li>
                <li class="col-md-3">
                    <div class="gallery-box">
                        <div class="frame">
                            <a href="#"><img src="<?php echo base_url(); ?>assets/front/images/gallety-4-column/gallery-4-column-img-7.jpg" alt="img"></a>
                            <div class="caption"><a href="<?php echo base_url(); ?>assets/front/images/gallety-4-column/gallery-4-column-img-7.jpg" data-rel="prettyPhoto[gallery1]"><i class="fa fa-search-plus"></i></a></div>
                        </div>
                        <div class="text-box">
                            <h2>Project Title #2</h2>
                            <p>Etiam porta sem malesuada magnais llis euismod. Aenean lacinia bibendui nec id elit non Aenean lacinia bibendui nec id elit non mi porta donec....</p>
                        </div>
                    </div>
                </li>
                <li class="col-md-3">
                    <div class="gallery-box">
                        <div class="frame">
                            <a href="#"><img src="<?php echo base_url(); ?>assets/front/images/gallety-4-column/gallery-4-column-img-8.jpg" alt="img"></a>
                            <div class="caption"><a href="<?php echo base_url(); ?>assets/front/images/gallety-4-column/gallery-4-column-img-8.jpg" data-rel="prettyPhoto[gallery1]"><i class="fa fa-search-plus"></i></a></div>
                        </div>
                        <div class="text-box">
                            <h2>Project Title #2</h2>
                            <p>Etiam porta sem malesuada magnais llis euismod. Aenean lacinia bibendui nec id elit non Aenean lacinia bibendui nec id elit non mi porta donec....</p>
                        </div>
                    </div>
                </li>
            </ul>

            <div class="pagination-area">
                <div class="pagination">
                    <ul>
                        <li class="active"><a href="#">01</a></li>
                        <li><a href="#">02</a></li>
                        <li><a href="#">03</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </section>

</div>