
        <div id="main">
            <section class="generic-heading-3">
                <div class="container">
                    <h1>How We Work</h1>
                    <strong class="title-line">conserve the lands and waters on which all life depends</strong> </div>
            </section>

            <section class="how-we-work">
                <div class="container">
                    <div class="work-banner">
                        <ul id="work-banner">
                            <li>
                                <div class="frame"> <img src="<?php echo base_url(); ?>assets/front/images/how-work/our-work-img-1.jpg" alt="img">
                                    <div class="caption">
                                        <div class="left"> <strong class="title">The Project Title</strong> <strong class="date">Published: October 14, 2013</strong> </div>
                                        <div class="right"> <a href="#" class="zoom"><i class="fa fa-eye"></i></a> <a href="#" class="search"><i class="fa fa-search"></i></a> </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="frame"> <img src="<?php echo base_url(); ?>assets/front/images/how-work/our-work-img-2.jpg" alt="img">
                                    <div class="caption">
                                        <div class="left"> <strong class="title">The Project Title</strong> <strong class="date">Published: October 14, 2013</strong> </div>
                                        <div class="right"> <a href="#" class="zoom"><i class="fa fa-eye"></i></a> <a href="#" class="search"><i class="fa fa-search"></i></a> </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="work-text-box">
                        <h2>Vestibulum id ligula porta felis euismod semper.</h2>
                        <p>Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis. Donec id elit non mi porta gravida at eget metus. Lorem ipsum do lor sit amet, consectetur adipiscing elit. Cras mattis consectetur purus sit am et fermentum.Aenean lacinia bibendum nulla sed.</p>
                        <div class="row-fluid">
                            <div class="col-md-3">
                                <div class="detail-box">
                                    <h3>Plant Trees</h3>
                                    <strong class="title">Morbi interdum sapien</strong>
                                    <ul>
                                        <li><i class="fa fa-chevron-right"></i>Aliquam tincidunt mauris</li>
                                        <li><i class="fa fa-chevron-right"></i>Vestibulum auctor dap</li>
                                        <li><i class="fa fa-chevron-right"></i>Nunc dignissim risus id</li>
                                        <li><i class="fa fa-chevron-right"></i>Vivamus vestibulum nu</li>
                                        <li><i class="fa fa-chevron-right"></i>Praesent placerat risus qu</li>
                                        <li><i class="fa fa-chevron-right"></i>Fusce pellentesque susc</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="detail-box">
                                    <h3>Fight Hunger</h3>
                                    <strong class="title">Suspendisse mauris amet</strong>
                                    <ul>
                                        <li><i class="fa fa-chevron-right"></i>Vestibulum commodo</li>
                                        <li><i class="fa fa-chevron-right"></i>Ut aliquam sollicitudin</li>
                                        <li><i class="fa fa-chevron-right"></i>Cras iaculis ultricies nul</li>
                                        <li><i class="fa fa-chevron-right"></i>Donec quis dui at dolor</li>
                                        <li><i class="fa fa-chevron-right"></i>Vivamus molestie gravida</li>
                                        <li><i class="fa fa-chevron-right"></i>Fusce lobortis lorem at</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="detail-box">
                                    <h3>Opportunity</h3>
                                    <strong class="title">Nunc sem lacus integer</strong>
                                    <ul>
                                        <li><i class="fa fa-chevron-right"></i>Aliquam tincidunt mauris</li>
                                        <li><i class="fa fa-chevron-right"></i>Vestibulum auctor dap</li>
                                        <li><i class="fa fa-chevron-right"></i>Nunc dignissim risus id</li>
                                        <li><i class="fa fa-chevron-right"></i>Vivamus vestibulum nu</li>
                                        <li><i class="fa fa-chevron-right"></i>Praesent placerat risus qu</li>
                                        <li><i class="fa fa-chevron-right"></i>Fusce pellentesque susc</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="detail-box">
                                    <h3>Improve Health</h3>
                                    <strong class="title">Nunc sem lacus integer</strong>
                                    <ul>
                                        <li><i class="fa fa-chevron-right"></i>Aliquam tincidunt mauris</li>
                                        <li><i class="fa fa-chevron-right"></i>Vestibulum auctor dap</li>
                                        <li><i class="fa fa-chevron-right"></i>Nunc dignissim risus id</li>
                                        <li><i class="fa fa-chevron-right"></i>Vivamus vestibulum nu</li>
                                        <li><i class="fa fa-chevron-right"></i>Praesent placerat risus qu</li>
                                        <li><i class="fa fa-chevron-right"></i>Fusce pellentesque susc</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="view-section">
                        <ul class="row gallery">
                            <li class="col-md-6">
                                <div class="box-1">
                                    <div class="frame">
                                        <a href="images/how-work/view-img-1.jpg" data-rel="prettyPhoto[gallery1]"><img src="<?php echo base_url(); ?>assets/front/images/how-work/view-img-1.jpg" alt="img"></a>
                                    </div>
                                    <div class="text-box">
                                        <h3>Plant Trees</h3>
                                        <div class="right-text"> <strong class="mnt">DEC 2013</strong> <a href="#" class="admin">Written by admin</a> </div>
                                        <p>Lorem ipsum dolor sit amet, consecteter ad elit sed diam nonummy ni euismod tincidunt ut laoreet dolore mag. Lorem ipsumdolor sit amet da varum avi dolor sit amet venit lorem ipsum consecteter diam.</p>
                                        <a href="#" class="readmore">Read more</a> </div>
                                </div>
                            </li>
                            <li class="col-md-6">
                                <div class="box-1">
                                    <div class="frame">
                                        <a href="images/how-work/view-img-2.jpg" data-rel="prettyPhoto[gallery1]"><img src="<?php echo base_url(); ?>assets/front/images/how-work/view-img-2.jpg" alt="img"></a>
                                    </div>
                                    <div class="text-box">
                                        <h3>Fight Hunger</h3>
                                        <div class="right-text"> <strong class="mnt">DEC 2013</strong> <a href="#" class="admin">Written by admin</a> </div>
                                        <p>Lorem ipsum dolor sit amet, consecteter ad elit sed diam nonummy ni euismod tincidunt ut laoreet dolore mag. Lorem ipsumdolor sit amet da varum avi dolor sit amet venit lorem ipsum consecteter diam.</p>
                                        <a href="#" class="readmore">Read more</a> </div>
                                </div>
                            </li>
                            <li class="col-md-6">
                                <div class="box-1">
                                    <div class="frame">
                                        <a href="images/how-work/view-img-3.jpg" data-rel="prettyPhoto[gallery1]"><img src="<?php echo base_url(); ?>assets/front/images/how-work/view-img-3.jpg" alt="img"></a>
                                    </div>
                                    <div class="text-box">
                                        <h3>Economic Opportunity</h3>
                                        <div class="right-text"> <strong class="mnt">DEC 2013</strong> <a href="#" class="admin">Written by admin</a> </div>
                                        <p>Lorem ipsum dolor sit amet, consecteter ad elit sed diam nonummy ni euismod tincidunt ut laoreet dolore mag. Lorem ipsumdolor sit amet da varum avi dolor sit amet venit lorem ipsum consecteter diam.</p>
                                        <a href="#" class="readmore">Read more</a> </div>
                                </div>
                            </li>
                            <li class="col-md-6">
                                <div class="box-1">
                                    <div class="frame">
                                        <a href="images/how-work/view-img-4.jpg" data-rel="prettyPhoto[gallery1]"><img src="<?php echo base_url(); ?>assets/front/images/how-work/view-img-4.jpg" alt="img"></a>
                                    </div>
                                    <div class="text-box">
                                        <h3>Mobilize the Church</h3>
                                        <div class="right-text"> <strong class="mnt">DEC 2013</strong> <a href="#" class="admin">Written by admin</a> </div>
                                        <p>Lorem ipsum dolor sit amet, consecteter ad elit sed diam nonummy ni euismod tincidunt ut laoreet dolore mag. Lorem ipsumdolor sit amet da varum avi dolor sit amet venit lorem ipsum consecteter diam.</p>
                                        <a href="#" class="readmore">Read more</a> </div>
                                </div>
                            </li>
                        </ul>
                        <a href="#" class="view">View More </a> </div>
                </div>
            </section>

        </div>
