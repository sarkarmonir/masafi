    <!--Main Slider-->
    <section class="main-slider">
        
        <div class="tp-banner-container">
            <div class="tp-banner">
                <ul>
                    
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="<?=base_url()?>assets/images/main-slider/image-1.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                    <img src="<?=base_url()?>assets/images/main-slider/s1.png"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="-100"
                    data-speed="1500"
                    data-start="0"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><h2>Masafi Group <br>Bangladesh</h2></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="10"
                    data-speed="1500"
                    data-start="500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                     data-endeasing="Power4.easeIn"><div class="text text-center">To manufacture products comparable to international standards <br>to be customer-focused and globally competitive through better quality, latest technology and continuous innovation.</div></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="110"
                    data-speed="1500"
                    data-start="1000"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-one">Sign Up</a></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="left" data-hoffset="170"
                    data-y="center" data-voffset="110"
                    data-speed="1500"
                    data-start="1500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-two">Learn More</a></div>
                    
                    
                    </li>
                    
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="<?=base_url()?>assets/images/main-slider/image-2.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                    <img src="<?=base_url()?>assets/images/main-slider/s2.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="center" data-hoffset="15"
                    data-y="center" data-voffset="-80"
                    data-speed="1500"
                    data-start="0"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><h2>Masafi Group Bangladesh</h2></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="center" data-hoffset="15"
                    data-y="center" data-voffset="0"
                    data-speed="1500"
                    data-start="500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><div class="text text-center">To manufacture products comparable to international standards <br>to be customer-focused and globally competitive through better quality, latest technology and continuous innovation.</div></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="center" data-hoffset="-80"
                    data-y="center" data-voffset="90"
                    data-speed="1500"
                    data-start="1000"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-one">Sign Up</a></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="center" data-hoffset="90"
                    data-y="center" data-voffset="90"
                    data-speed="1500"
                    data-start="1500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-two">Learn More</a></div>
                    
                    
                    </li>
                    
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="<?=base_url()?>assets/images/main-slider/image-3.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                    <img src="<?=base_url()?>assets/images/main-slider/s3.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="center" data-hoffset="15"
                    data-y="center" data-voffset="-80"
                    data-speed="1500"
                    data-start="0"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><h2>Shirin Spinning Mill</h2></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="center" data-hoffset="15"
                    data-y="center" data-voffset="0"
                    data-speed="1500"
                    data-start="500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><div class="text text-center">To manufacture products comparable to international standards <br>to be customer-focused and globally competitive through better quality, latest technology and continuous innovation.</div></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="right" data-hoffset="-15"
                    data-y="center" data-voffset="110"
                    data-speed="1500"
                    data-start="1000"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-one">Sign Up</a></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="right" data-hoffset="-170"
                    data-y="center" data-voffset="110"
                    data-speed="1500"
                    data-start="1500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-two">Learn More</a></div>
                    
                    
                    </li>


                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="<?=base_url()?>assets/images/main-slider/image-2.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                    <img src="<?=base_url()?>assets/images/main-slider/s4.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="center" data-hoffset="15"
                    data-y="center" data-voffset="-80"
                    data-speed="1500"
                    data-start="0"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><h2>AB.R-Spinning Mills Ltd.</h2></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="center" data-hoffset="15"
                    data-y="center" data-voffset="0"
                    data-speed="1500"
                    data-start="500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><div class="text text-center">To manufacture products comparable to international standards <br>to be customer-focused and globally competitive through better quality, latest technology and continuous innovation.</div></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="center" data-hoffset="-80"
                    data-y="center" data-voffset="90"
                    data-speed="1500"
                    data-start="1000"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-one">Sign Up</a></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="center" data-hoffset="90"
                    data-y="center" data-voffset="90"
                    data-speed="1500"
                    data-start="1500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-two">Learn More</a></div>
                    
                    
                    </li>
                    
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="<?=base_url()?>assets/images/main-slider/image-3.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                    <img src="<?=base_url()?>assets/images/main-slider/s5.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                   <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="center" data-hoffset="15"
                    data-y="center" data-voffset="-80"
                    data-speed="1500"
                    data-start="0"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><h2>Masafi Bread & Biscuit Ind. Ltd.</h2></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="center" data-hoffset="15"
                    data-y="center" data-voffset="0"
                    data-speed="1500"
                    data-start="500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><div class="text text-center">To manufacture products comparable to international standards <br>to be customer-focused and globally competitive through better quality, latest technology and continuous innovation.</div></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="right" data-hoffset="-15"
                    data-y="center" data-voffset="110"
                    data-speed="1500"
                    data-start="1000"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-one">Sign Up</a></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="right" data-hoffset="-170"
                    data-y="center" data-voffset="110"
                    data-speed="1500"
                    data-start="1500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-two">Learn More</a></div>
                    
                    
                    </li>
                    
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="<?=base_url()?>assets/images/main-slider/image-1.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                    <img src="<?=base_url()?>assets/images/main-slider/s6.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="-100"
                    data-speed="1500"
                    data-start="0"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><h2>Masafi Bread & Biscuit Ind. Ltd.</div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="10"
                    data-speed="1500"
                    data-start="500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                     data-endeasing="Power4.easeIn"><div class="text text-center">To manufacture products comparable to international standards <br>to be customer-focused and globally competitive through better quality, latest technology and continuous innovation.</div></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="110"
                    data-speed="1500"
                    data-start="1000"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-one">Sign Up</a></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="left" data-hoffset="170"
                    data-y="center" data-voffset="110"
                    data-speed="1500"
                    data-start="1500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-two">Learn More</a></div>
                    
                    
                    </li>
                    
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="<?=base_url()?>assets/images/main-slider/image-2.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                    <img src="<?=base_url()?>assets/images/main-slider/s7.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="center" data-hoffset="15"
                    data-y="center" data-voffset="-80"
                    data-speed="1500"
                    data-start="0"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><h2>Apon Food Products Ltd.</h2></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="center" data-hoffset="15"
                    data-y="center" data-voffset="0"
                    data-speed="1500"
                    data-start="500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                     data-endeasing="Power4.easeIn"><div class="text text-center">To manufacture products comparable to international standards <br>to be customer-focused and globally competitive through better quality, latest technology and continuous innovation.</div></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="center" data-hoffset="-80"
                    data-y="center" data-voffset="90"
                    data-speed="1500"
                    data-start="1000"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-one">Sign Up</a></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="center" data-hoffset="90"
                    data-y="center" data-voffset="90"
                    data-speed="1500"
                    data-start="1500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-two">Learn More</a></div>
                    
                    
                    </li>
                    
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="<?=base_url()?>assets/images/main-slider/image-3.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                    <img src="<?=base_url()?>assets/images/main-slider/s8.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="right" data-hoffset="-15"
                    data-y="center" data-voffset="-100"
                    data-speed="1500"
                    data-start="0"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><h2 class="text-right">Apon Food Products Ltd.</h2></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="right" data-hoffset="-15"
                    data-y="center" data-voffset="10"
                    data-speed="1500"
                    data-start="500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                     data-endeasing="Power4.easeIn"><div class="text text-center">To manufacture products comparable to international standards <br>to be customer-focused and globally competitive through better quality, latest technology and continuous innovation.</div></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="right" data-hoffset="-15"
                    data-y="center" data-voffset="110"
                    data-speed="1500"
                    data-start="1000"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-one">Sign Up</a></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="right" data-hoffset="-170"
                    data-y="center" data-voffset="110"
                    data-speed="1500"
                    data-start="1500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-two">Learn More</a></div>
                    
                    
                    </li>


                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="<?=base_url()?>assets/images/main-slider/image-2.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                    <img src="<?=base_url()?>assets/images/main-slider/s9.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="center" data-hoffset="15"
                    data-y="center" data-voffset="-80"
                    data-speed="1500"
                    data-start="0"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><h2 class="text-right">Apon Food Products Ltd.</h2></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="center" data-hoffset="15"
                    data-y="center" data-voffset="0"
                    data-speed="1500"
                    data-start="500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                     data-endeasing="Power4.easeIn"><div class="text text-center">To manufacture products comparable to international standards <br>to be customer-focused and globally competitive through better quality, latest technology and continuous innovation.</div></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="center" data-hoffset="-80"
                    data-y="center" data-voffset="90"
                    data-speed="1500"
                    data-start="1000"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-one">Sign Up</a></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="center" data-hoffset="90"
                    data-y="center" data-voffset="90"
                    data-speed="1500"
                    data-start="1500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-two">Learn More</a></div>
                    
                    
                    </li>
                    
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="<?=base_url()?>assets/images/main-slider/image-3.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                    <img src="<?=base_url()?>assets/images/main-slider/s10.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="right" data-hoffset="-15"
                    data-y="center" data-voffset="-100"
                    data-speed="1500"
                    data-start="0"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><h2 class="text-right">Masafi Flour Mills Ltd.</h2></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="right" data-hoffset="-15"
                    data-y="center" data-voffset="10"
                    data-speed="1500"
                    data-start="500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                     data-endeasing="Power4.easeIn"><div class="text text-center">To manufacture products comparable to international standards <br>to be customer-focused and globally competitive through better quality, latest technology and continuous innovation.</div></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="right" data-hoffset="-15"
                    data-y="center" data-voffset="110"
                    data-speed="1500"
                    data-start="1000"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-one">Sign Up</a></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="right" data-hoffset="-170"
                    data-y="center" data-voffset="110"
                    data-speed="1500"
                    data-start="1500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-two">Learn More</a></div>
                    
                    
                    </li>
                    
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="<?=base_url()?>assets/images/main-slider/image-2.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                    <img src="<?=base_url()?>assets/images/main-slider/s11.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="center" data-hoffset="15"
                    data-y="center" data-voffset="-80"
                    data-speed="1500"
                    data-start="0"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><h2>Masafi Flour Mills Ltd.</h2></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="center" data-hoffset="15"
                    data-y="center" data-voffset="0"
                    data-speed="1500"
                    data-start="500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                     data-endeasing="Power4.easeIn"><div class="text text-center">To manufacture products comparable to international standards <br>to be customer-focused and globally competitive through better quality, latest technology and continuous innovation.</div></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="center" data-hoffset="-80"
                    data-y="center" data-voffset="90"
                    data-speed="1500"
                    data-start="1000"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-one">Sign Up</a></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="center" data-hoffset="90"
                    data-y="center" data-voffset="90"
                    data-speed="1500"
                    data-start="1500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-two">Learn More</a></div>
                    
                    
                    </li>
                    
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="<?=base_url()?>assets/images/main-slider/image-3.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                    <img src="<?=base_url()?>assets/images/main-slider/s12.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="right" data-hoffset="-15"
                    data-y="center" data-voffset="-100"
                    data-speed="1500"
                    data-start="0"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><h2>Masafi Group <br>Bangladesh</h2></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme"
                    data-x="right" data-hoffset="-15"
                    data-y="center" data-voffset="10"
                    data-speed="1500"
                    data-start="500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                     data-endeasing="Power4.easeIn"><div class="text text-center">To manufacture products comparable to international standards <br>to be customer-focused and globally competitive through better quality, latest technology and continuous innovation.</div></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="right" data-hoffset="-15"
                    data-y="center" data-voffset="110"
                    data-speed="1500"
                    data-start="1000"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-one">Sign Up</a></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme"
                    data-x="right" data-hoffset="-170"
                    data-y="center" data-voffset="110"
                    data-speed="1500"
                    data-start="1500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-two">Learn More</a></div>
                    
                    
                    </li>
                    
                </ul>
                
                <div class="tp-bannertimer"></div>
            </div>
        </div>
    </section>
    <!--About Us-->
     <section class="about-section">
          <div class="auto-container">
               <div class="row clearfix">
                    <!--Section Title-->
                    <div class="sec-title">
                         <h2>Welcome to <strong>Masafi Group</strong></h2>
                    </div>
                    <div class="col-md-7 col-sm-6 col-xs-12">
                         <div class="content-outer">
                             <div class="text" style="text-align: justify">Masafi Group has been established in 2000 as a Trading company as well as manufacturing

company of Yarn Spinning and Agro based food manufacturing industry. The sister

concern of Masafi Group are Shirin Spinning Mills Ltd. (Unit-1 &amp; Unit-2), AB.R Spinning

Mills Ltd., Shirin Rotor, Masafi Bread and Biscuit Industries Ltd., Dimond Biscuit Ltd.,

Masafi Agro Food Industries ltd., Masafi Trade International. All of our manufacturing

industries are located in a single belt at Gorai, Mirzapur, Tangail. Masafi Group always

produce world class product to satisfy its esteemed patrons and associates. The fame of our

product and production units have expressed across the world like Asia, Middle East,

Europe, Africa and North American countries as well. Our produced Yarn is well reputed in

RMG sector in Bangladesh. We are producing fresh, delicious and nutritious organic food in

local food market. The Goal of Masafi Group is to be a leader of Yarn and Agro based food

manufacturing company in Bangladesh through ensuring the best product quality and

outstanding evaluation through ultimate customer satisfaction. Beside quality product we

are committed to produce corporate leaders with ensuring a large number of

employments. <br><br>

Trust, Customer Association and Focus, Product Excellence are the main core values of

Masafi Group. Because we believe that regarding customer satisfaction, Quality and

Commitment - “Down to Dusk”.</div>
                              
                         </div>
                    </div>
                    <div class="col-md-5 col-sm-6 col-xs-12">
                         <div class="content-outer">
                              <iframe width="530" height="439" src="https://www.youtube.com/embed/ybTxnc76Wc0" frameborder="0" allowfullscreen></iframe>
                         </div>
                    </div>
               </div>
          </div>
     </section>

    <!--Call To Action-->
    <section class="call-to-action">
        <div class="outer">
            <div class="inner">
                <div class="layer-left"></div>
                <div class="layer-right"></div>
                <strong>More than thousands customers we served. It’s your time.</strong>
                <a href="#" class="theme-btn">Join us</a>
            </div>
        </div>
    </section>
    <!--Why Choose Us-->
     <section class="why-chooose-us">
         <div class="auto-container">
          <!--Section Title-->
          <div class="sec-title">
               <h2>Why Choose <strong>Us</strong></h2>
          </div>


              


            <div class="row clearfix">
                <!--Featured Project Column-->
                <div class="featured-project-column col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image-box image-box-x"><a href="#"><img src="<?=base_url()?>assets/images/resource/a1.jpg" alt=""></a>
                        <a href="#" class="theme-btnx">Video Gallery</a></figure>
                    </div>
                </div>
                
                <!--Featured Project Column-->
                <div class="featured-project-column col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <figure class="image-box image-box-x"><a href="#"><img src="<?=base_url()?>assets/images/resource/a2.png" alt=""></a>
                        <a href="#" class="theme-btnx ">Photo Gallery</a></figure>
                    </div>
                </div>
                
                <!--Featured Project Column-->
                <div class="featured-project-column col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <figure class="image-box image-box-x"><a href="#"><img src="<?=base_url()?>assets/images/resource/a3.jpg" alt=""></a>
                        <a href="#" class="theme-btnx ">Career</a></figure>
                    </div>
                </div>
                
                <!--Featured Project Column-->
                <div class="featured-project-column col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image-box image-box-x"><a href="#"><img src="<?=base_url()?>assets/images/resource/a4.png"  alt=""></a>
                        <a href="#" class="theme-btnx ">Factory Location</a></figure>
                    </div>
                </div>
                
                <!--Featured Project Column-->
                <div class="featured-project-column col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <figure class="image-box image-box-x"><a href="#"><img src="<?=base_url()?>assets/images/resource/a5.jpg"  alt=""></a>
                        <a href="#" class="theme-btnx ">Head Office</a></figure>
                    </div>
                </div>
                
                <!--Featured Project Column-->
                <div class="featured-project-column col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <figure class="image-box image-box-x"><a href="#"><img src="<?=base_url()?>assets/images/resource/a6.jpg" alt=""></a>
                        <a href="#" class="theme-btnx ">Latest News</a></figure>
                    </div>
                </div>
                
            </div>
            
            
     </div>
    </section>

