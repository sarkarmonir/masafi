        <div id="main">
            <section class="generic-heading-3">
                <div class="container">
                    <h1>Shirin Spinning Mills Ltd.</h1>
                    <strong class="title-line">When you want the best.....</strong> </div>
                </section>

                <section class="about-page">
                    <section class="about-section-1">
                        <div class="container">
                            <div class="row-fluid">
                                <div class="col-md-3">
                                <h2 class="title-opt">Concern Name</h2>
                                <div class="about-box-1">
                                        <nav class="nav-sidebar">
                                            <ul class="nav tabs">
                                              <li class="active"><a href="#tab1" data-toggle="tab">Lorem ipsum</a></li>
                                              <li class=""><a href="#tab2" data-toggle="tab">Dolor asit amet</a></li>
                                              <li class=""><a href="#tab3" data-toggle="tab">Stet clita</a></li>
                                              <li class=""><a href="#tab4" data-toggle="tab">Stet clita</a></li>   
                                              <li class=""><a href="#tab5" data-toggle="tab">Process</a></li>                            
                                          </ul>
                                      </nav>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                <h2 class="title-opt">Concern Details</h2>
                                <div class="about-box-1">
                                  <!-- tab content -->
                                  <div class="tab-content">
                                    <div class="tab-pane active text-style" id="tab1">
                                      <h2>Lorem ipsum</h2>
                                      <p>
                                         Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's 
                                         standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make 
                                         a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining 
                                         essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, 
                                         and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.  
                                     </p>
                                     <hr>
                                     <img src="http://placehold.it/350x250" class="img-rounded pull-right">   
                                 </div>
                                 <div class="tab-pane text-style" id="tab2">
                                  <h2>Dolor asit amet</h2>
                                  <p>Dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt 
                                    ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo 
                                    dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. 
                                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore 
                                    et dolore magna aliquyam erat, sed diam voluptua.</p>
                                    <hr>
                                    <img src="http://placehold.it/150x90" class="img-rounded pull-left">
                                </div>
                                <div class="tab-pane text-style" id="tab3">
                                  <h2>Stet clita</h2>
                                  <p>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum 
                                    iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla 
                                    facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit 
                                    augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet,</p>
                                    <hr>
                                    <div class="col-xs-6 col-md-3">
                                      <img src="http://placehold.it/150x150" class="img-rounded pull-right">
                                  </div>
                              </div>
                                  <div class="tab-pane text-style" id="tab4">
                                  <h2>Stet clita</h2>
                                  <p>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum 
                                    iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla 
                                    facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit 
                                    augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet,</p>
                                    <hr>
                                    <div class="col-xs-6 col-md-3">
                                      <img src="http://placehold.it/150x150" class="img-rounded pull-right">
                                  </div>
                              </div>
                                  <div class="tab-pane text-style" id="tab5">
                                  <h2>Stet clita</h2>
                                  <p>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum 
                                    iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla 
                                    facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit 
                                    augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet,</p>
                                    <hr>
                                    <div class="col-xs-12 col-md-12">
                                    <iframe width="100%" height="400" src="https://www.youtube.com/embed/YYWlevX7Kw0" frameborder="0" allowfullscreen></iframe>

                                  </div>
                              </div>
                          </div>


                      </div>
                  </div>
              <div class="col-md-3">
                                <h2 class="title-opt">Contact Details</h2>
                                <div class="about-box-1">
                                        <ul class="nav tabs">
                                              <li class=""><a href="#tab1" data-toggle="tab">Lorem ipsum</a></li>
                                              <li class=""><a href="#tab2" data-toggle="tab">Dolor asit amet</a></li>
                                              <li class=""><a href="#tab3" data-toggle="tab">Stet clita</a></li>
                                              <li class=""><a href="#tab4" data-toggle="tab">Stet clita</a></li>   
                                              <li class=""><a href="#tab5" data-toggle="tab">Process</a></li>                            
                                          </ul>
                                  </div>
                              </div>
              </div>
          </div>
      </div>
  </section>

</section>

</div>
