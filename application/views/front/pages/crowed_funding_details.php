
        <div id="main">
            <section class="generic-heading-3">
                <div class="container">
                    <h1>Fund Raising Projects</h1>
                    <strong class="title-line">conserve the lands and waters on which all life depends</strong> </div>
            </section>

            <section class="funding-detail">
                <div class="container">
                    <div class="frame">
                        <a href="#"><img src="<?php echo base_url(); ?>assets/front/images/funding/funding-detail-img-1.jpg" alt="img"></a>
                        <div class="caption">
                            <h2>Urgent Cause Stop Syria’s Hunger</h2>
                            <div class="funding-detail-progress"> <strong class="amount">$12,265 </strong> <strong class="goal">Pledged of $200,000 Goal</strong>
                                <div class="progress progress-striped active">
                                    <div class="bar" style="width: 40%;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bottom-row"> <a href="#" class="btn-detail">677 Pledgers</a> <a href="#" class="btn-detail">135 Days Left</a> <a href="#" class="btn-detail">Donate</a> </div>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. </p>
                    <strong class="text">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecaticupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</strong>
                    <p>dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>
                    <div class="funding-detail-map-box">
                        <div id="map_contact_4" class="map_canvas active"></div>
                        <div class="inner"> <i class="fa fa-map-marker"></i> <strong class="address">Campaign Launched on 25 January, 2014
at <a href="#">LONDON, UK</a></strong> <strong class="title">share THIS CAMPAIGN</strong>
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
                                <li><a href="#"><i class="fa fa-comment-o"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="donation-rank-box"> <a href="#" class="btn-donation">Donation Rank</a>
                        <ul>
                            <li>
                                <div class="rank-box"><strong class="rank">Rank 1 : $1 to </strong></div>
                                <div class="donate-box-2"><strong class="title">Maecenas sed diam eget risus varius blandit sit amet non magna. Donec ullamcorper nulla non metus auctor fringilla. Vestibulum </strong><a href="#" class="btn-donate">Donate</a></div>
                            </li>
                            <li>
                                <div class="rank-box"><strong class="rank">Rank 2 : $50</strong></div>
                                <div class="donate-box-2"><strong class="title">Maecenas sed diam eget risus varius blandit sit amet non magna porta felis euismod semper....</strong><a href="#" class="btn-donate">Donate</a></div>
                            </li>
                            <li>
                                <div class="rank-box"><strong class="rank">Rank 3 : $100 to </strong></div>
                                <div class="donate-box-2"><strong class="title">porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla. Nullam quis risus eget urna mollis ornare vel..</strong><a href="#" class="btn-donate">Donate</a></div>
                            </li>
                            <li>
                                <div class="rank-box"><strong class="rank">Rank 4 : $500 to </strong></div>
                                <div class="donate-box-2"><strong class="title">Everyone has an adoring fan somewhere. That person is you.</strong><a href="#" class="btn-donate">Donate</a></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>

        </div>
