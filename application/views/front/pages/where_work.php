<div id="main">
    <section class="generic-heading-3">
        <div class="container">
            <h1>Where We Work</h1>
            <strong class="title-line">conserve the lands and waters on which all life depends</strong> </div>
    </section>

    <section class="how-we-work">
        <div class="container">
            <div class="view-section">
                <ul class="row gallery">
                    <li class="col-md-6">
                        <div class="box-1">
                            <div class="frame">
                                <a href="images/how-work/where-work-img-1.jpg" data-rel="prettyPhoto[gallery1]"><img src="<?php echo base_url(); ?>assets/front/images/how-work/where-work-img-1.jpg" alt="img"></a>
                            </div>
                            <div class="text-box">
                                <h3>Dominican Republic</h3>
                                <div class="right-text"> <strong class="mnt">DEC 2013</strong> <a href="#" class="admin">Written by admin</a> </div>
                                <p>Lorem ipsum dolor sit amet, consecteter ad elit sed diam nonummy ni euismod tincidunt ut laoreet dolore mag. Lorem ipsumdolor sit amet da varum avi dolor sit amet venit lorem ipsum consecteter diam.</p>
                                <a href="#" class="readmore">Read more</a> </div>
                        </div>
                    </li>
                    <li class="col-md-6">
                        <div class="box-1">
                            <div class="frame">
                                <a href="images/how-work/where-work-img-2.jpg" data-rel="prettyPhoto[gallery1]"><img src="<?php echo base_url(); ?>assets/front/images/how-work/where-work-img-2.jpg" alt="img"></a>
                            </div>
                            <div class="text-box">
                                <h3>Tanzania</h3>
                                <div class="right-text"> <strong class="mnt">DEC 2013</strong> <a href="#" class="admin">Written by admin</a> </div>
                                <p>Lorem ipsum dolor sit amet, consecteter ad elit sed diam nonummy ni euismod tincidunt ut laoreet dolore mag. Lorem ipsumdolor sit amet da varum avi dolor sit amet venit lorem ipsum consecteter diam.</p>
                                <a href="#" class="readmore">Read more</a> </div>
                        </div>
                    </li>
                    <li class="col-md-6">
                        <div class="box-1">
                            <div class="frame">
                                <a href="images/how-work/where-work-img-3.jpg" data-rel="prettyPhoto[gallery1]"><img src="<?php echo base_url(); ?>assets/front/images/how-work/where-work-img-3.jpg" alt="img"></a>
                            </div>
                            <div class="text-box">
                                <h3>Thailand</h3>
                                <div class="right-text"> <strong class="mnt">DEC 2013</strong> <a href="#" class="admin">Written by admin</a> </div>
                                <p>Lorem ipsum dolor sit amet, consecteter ad elit sed diam nonummy ni euismod tincidunt ut laoreet dolore mag. Lorem ipsumdolor sit amet da varum avi dolor sit amet venit lorem ipsum consecteter diam.</p>
                                <a href="#" class="readmore">Read more</a> </div>
                        </div>
                    </li>
                    <li class="col-md-6">
                        <div class="box-1">
                            <div class="frame">
                                <a href="images/how-work/where-work-img-4.jpg" data-rel="prettyPhoto[gallery1]"><img src="<?php echo base_url(); ?>assets/front/images/how-work/where-work-img-4.jpg" alt="img"></a>
                            </div>
                            <div class="text-box">
                                <h3>Mexico</h3>
                                <div class="right-text"> <strong class="mnt">DEC 2013</strong> <a href="#" class="admin">Written by admin</a> </div>
                                <p>Lorem ipsum dolor sit amet, consecteter ad elit sed diam nonummy ni euismod tincidunt ut laoreet dolore mag. Lorem ipsumdolor sit amet da varum avi dolor sit amet venit lorem ipsum consecteter diam.</p>
                                <a href="#" class="readmore">Read more</a> </div>
                        </div>
                    </li>
                </ul>
                <!-- <a href="#" class="view">View More </a>  --></div>
        </div>
    </section>

</div>