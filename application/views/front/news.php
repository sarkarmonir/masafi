    
    <!--Blog News Section-->
    <section class="news-section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-3 col-md-12 col-xs-12">
                    <!--Section Title-->
                    <div class="sec-title left-aligned">
                        <h2>Latest <strong>News</strong></h2>
                    </div>
                </div>
                
                <div class="col-lg-9 col-md-12 col-xs-12">    
                    <!--Desc Text-->
                    <div class="desc-text">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.</div>
                </div>
            </div>
          
            <div class="row clearfix">
                <!--Default News Post-->
                <div class="default-news-post col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="<?=base_url()?>news_events/details"><img src="<?=base_url()?>assets/images/resource/blog-image-1.jpg" alt=""></a><a href="<?=base_url()?>news_events/details" class="theme-btn link-btn">Read More</a></figure>
                        <div class="lower-content">
                            <h3><a href="<?=base_url()?>news_events/details">Growth of finance around the third world. </a></h3>
                            <ul class="post-meta clearfix">
                                <li><a href="#">February 19</a></li>
                                <li><a href="#">Hasib Sharif</a></li>
                                <li><a href="#">Gardening</a></li> 
                                <li><a href="#"><span class="icon flaticon-multimedia-7"></span> 15</a></li>
                            </ul>
                            <div class="text">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classi cal Latin literature from 45 BC, making it over 2000 years old.</div>
                        </div>
                    </div>
                </div>
                <!--Default News Post-->
                <div class="default-news-post col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="<?=base_url()?>news_events/details"><img src="<?=base_url()?>assets/images/resource/blog-image-2.jpg" alt=""></a><a href="<?=base_url()?>news_events/details" class="theme-btn link-btn">Read More</a></figure>
                        <div class="lower-content">
                            <h3><a href="<?=base_url()?>news_events/details">World’s finest share their tips for success.</a></h3>
                            <ul class="post-meta clearfix">
                                <li><a href="#">February 19</a></li>
                                <li><a href="#">Hasib Sharif</a></li>
                                <li><a href="#">Gardening</a></li> 
                                <li><a href="#"><span class="icon flaticon-multimedia-7"></span> 15</a></li>
                            </ul>
                            <div class="text">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classi cal Latin literature from 45 BC, making it over 2000 years old.</div>
                        </div>
                    </div>
                </div>
                <!--Default News Post-->
                <div class="default-news-post col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="<?=base_url()?>news_events/details"><img src="<?=base_url()?>assets/images/resource/blog-image-3.jpg" alt=""></a><a href="<?=base_url()?>news_events/details" class="theme-btn link-btn">Read More</a></figure>
                        <div class="lower-content">
                            <h3><a href="<?=base_url()?>news_events/details">Fallen rate of stocks all over the world.</a></h3>
                            <ul class="post-meta clearfix">
                                <li><a href="#">February 19</a></li>
                                <li><a href="#">Hasib Sharif</a></li>
                                <li><a href="#">Gardening</a></li> 
                                <li><a href="#"><span class="icon flaticon-multimedia-7"></span> 15</a></li>
                            </ul>
                            <div class="text">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classi cal Latin literature from 45 BC, making it over 2000 years old.</div>
                        </div>
                    </div>
                </div>
                <!--Default News Post-->
                <div class="default-news-post col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="<?=base_url()?>news_events/details"><img src="<?=base_url()?>assets/images/resource/blog-image-4.jpg" alt=""></a><a href="<?=base_url()?>news_events/details" class="theme-btn link-btn">Read More</a></figure>
                        <div class="lower-content">
                            <h3><a href="<?=base_url()?>news_events/details">Growth of finance around the third world. </a></h3>
                            <ul class="post-meta clearfix">
                                <li><a href="#">February 19</a></li>
                                <li><a href="#">Hasib Sharif</a></li>
                                <li><a href="#">Gardening</a></li> 
                                <li><a href="#"><span class="icon flaticon-multimedia-7"></span> 15</a></li>
                            </ul>
                            <div class="text">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classi cal Latin literature from 45 BC, making it over 2000 years old.</div>
                        </div>
                    </div>
                </div>
                <!--Default News Post-->
                <div class="default-news-post col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="<?=base_url()?>news_events/details"><img src="<?=base_url()?>assets/images/resource/blog-image-5.jpg" alt=""></a><a href="<?=base_url()?>news_events/details" class="theme-btn link-btn">Read More</a></figure>
                        <div class="lower-content">
                            <h3><a href="<?=base_url()?>news_events/details">World’s finest share their tips for success.</a></h3>
                            <ul class="post-meta clearfix">
                                <li><a href="#">February 19</a></li>
                                <li><a href="#">Hasib Sharif</a></li>
                                <li><a href="#">Gardening</a></li> 
                                <li><a href="#"><span class="icon flaticon-multimedia-7"></span> 15</a></li>
                            </ul>
                            <div class="text">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classi cal Latin literature from 45 BC, making it over 2000 years old.</div>
                        </div>
                    </div>
                </div>
                <!--Default News Post-->
                <div class="default-news-post col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="<?=base_url()?>news_events/details"><img src="<?=base_url()?>assets/images/resource/blog-image-6.jpg" alt=""></a><a href="<?=base_url()?>news_events/details" class="theme-btn link-btn">Read More</a></figure>
                        <div class="lower-content">
                            <h3><a href="<?=base_url()?>news_events/details">Fallen rate of stocks all over the world.</a></h3>
                            <ul class="post-meta clearfix">
                                <li><a href="#">February 19</a></li>
                                <li><a href="#">Hasib Sharif</a></li>
                                <li><a href="#">Gardening</a></li> 
                                <li><a href="#"><span class="icon flaticon-multimedia-7"></span> 15</a></li>
                            </ul>
                            <div class="text">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classi cal Latin literature from 45 BC, making it over 2000 years old.</div>
                        </div>
                    </div>
                </div>
                <!--Default News Post-->
                <div class="default-news-post col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="<?=base_url()?>news_events/details"><img src="<?=base_url()?>assets/images/resource/blog-image-7.jpg" alt=""></a><a href="<?=base_url()?>news_events/details" class="theme-btn link-btn">Read More</a></figure>
                        <div class="lower-content">
                            <h3><a href="<?=base_url()?>news_events/details">Growth of finance around the third world. </a></h3>
                            <ul class="post-meta clearfix">
                                <li><a href="#">February 19</a></li>
                                <li><a href="#">Hasib Sharif</a></li>
                                <li><a href="#">Gardening</a></li> 
                                <li><a href="#"><span class="icon flaticon-multimedia-7"></span> 15</a></li>
                            </ul>
                            <div class="text">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classi cal Latin literature from 45 BC, making it over 2000 years old.</div>
                        </div>
                    </div>
                </div>
                <!--Default News Post-->
                <div class="default-news-post col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="<?=base_url()?>news_events/details"><img src="<?=base_url()?>assets/images/resource/blog-image-8.jpg" alt=""></a><a href="<?=base_url()?>news_events/details" class="theme-btn link-btn">Read More</a></figure>
                        <div class="lower-content">
                            <h3><a href="<?=base_url()?>news_events/details">World’s finest share their tips for success.</a></h3>
                            <ul class="post-meta clearfix">
                                <li><a href="#">February 19</a></li>
                                <li><a href="#">Hasib Sharif</a></li>
                                <li><a href="#">Gardening</a></li> 
                                <li><a href="#"><span class="icon flaticon-multimedia-7"></span> 15</a></li>
                            </ul>
                            <div class="text">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classi cal Latin literature from 45 BC, making it over 2000 years old.</div>
                        </div>
                    </div>
                </div>
                <!--Default News Post-->
                <div class="default-news-post col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="<?=base_url()?>news_events/details"><img src="<?=base_url()?>assets/images/resource/blog-image-9.jpg" alt=""></a><a href="<?=base_url()?>news_events/details" class="theme-btn link-btn">Read More</a></figure>
                        <div class="lower-content">
                            <h3><a href="<?=base_url()?>news_events/details">Fallen rate of stocks all over the world.</a></h3>
                            <ul class="post-meta clearfix">
                                <li><a href="#">February 19</a></li>
                                <li><a href="#">Hasib Sharif</a></li>
                                <li><a href="#">Gardening</a></li> 
                                <li><a href="#"><span class="icon flaticon-multimedia-7"></span> 15</a></li>
                            </ul>
                            <div class="text">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classi cal Latin literature from 45 BC, making it over 2000 years old.</div>
                        </div>
                    </div>
                </div>
                
            </div>
            
            <div class="text-center padd-top-20"><a href="<?=base_url()?>" class="theme-btn btn-style-three">Load More</a></div>
          
        </div>
    </section>