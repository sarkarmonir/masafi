    
    
    <!--Contact Section-->
    <section class="contact-section">
        <div class="auto-container">
            <div class="content-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
        
                <!--Contact Info-->
                <div class="contact-info">
                    <div class="sec-title"><h2>Contact <strong>US</strong></h2></div>
                    
                    <div class="row clearfix">
                        
                        <!--Info Column-->
                        <div class="info-column col-md-4 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="icon"><span class="flaticon-location"></span></div>
                                <h4>Find us</h4>
                                <ul>
                                    <li>5052 street, Cicero, Downtown <br>LA - 8619, USA</li>
                                </ul>
                            </div>
                        </div>
                        
                        <!--Info Column-->
                        <div class="info-column col-md-4 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="icon"><span class="flaticon-technology-1"></span></div>
                                <h4>Call us</h4>
                                <ul>
                                    <li>+190 9213 221 330</li>
                                    <li>+190 8613 919 110</li>
                                </ul>
                            </div>
                        </div>
                        
                        <!--Info Column-->
                        <div class="info-column col-md-4 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="icon"><span class="flaticon-symbol"></span></div>
                                <h4>Mail Us</h4>
                                <ul>
                                    <li>contact@bizpro.com</li>
                                    <li>solution@bizpro.com</li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                
                </div>
                
                <!--Form Container-->
                <div class="form-container">
                    
                    <!--Form-->
                    <div class="form">
                        <form id="contact-form" method="post" action="">
                            <div class="row clearfix">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="username" value="" placeholder="Your Name">
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" name="email" value="" placeholder="Your Email">
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="phone" value="" placeholder="Your Phone">
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="subject" value="" placeholder="Subject">
                                </div>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <textarea name="message" placeholder="Your Message.."></textarea>
                                </div>
                                
                                <div class="form-group text-center margin-top-30 col-lg-12 col-md-6 col-sm-12 col-xs-12">
                                    <button type="submit" class="theme-btn btn-style-one">Send</button>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
                
                
            </div>
        </div>
    </section>
    
    
    <!--Map Section-->
    <section class="map-section">
        <!--Map Container-->
        <div class="map-outer">
            <!--Map Canvas-->
            <div class="map-canvas" style="height: 550px;">
            
            </div>
            
        </div>
    </section>
    

