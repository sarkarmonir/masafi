    <!--Featured Projects Section-->
    <section class="featured-projects bg-lightgrey">
        <div class="auto-container">
            <!--Section Title-->
            <div class="sec-title">
                <h2>Featured <strong>Projects</strong></h2>
            </div>
            
            <div class="row clearfix">
                <!--Featured Project Column-->
                <div class="featured-project-column col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="#"><img src="<?=base_url()?>assets/images/resource/featured-image-1.jpg" alt=""></a><a href="#" class="theme-btn link-btn">View Project</a></figure>
                        <div class="lower-content">
                            <h4><a href="#">Point of using lorem ipsum </a></h4>
                            <div class="text">Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model.</div>
                        </div>
                    </div>
                </div>
                
                <!--Featured Project Column-->
                <div class="featured-project-column col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="#"><img src="<?=base_url()?>assets/images/resource/featured-image-2.jpg" alt=""></a><a href="#" class="theme-btn link-btn">View Project</a></figure>
                        <div class="lower-content">
                            <h4><a href="#">Point of using lorem ipsum </a></h4>
                            <div class="text">Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model.</div>
                        </div>
                    </div>
                </div>
                
                <!--Featured Project Column-->
                <div class="featured-project-column col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="#"><img src="<?=base_url()?>assets/images/resource/featured-image-3.jpg" alt=""></a><a href="#" class="theme-btn link-btn">View Project</a></figure>
                        <div class="lower-content">
                            <h4><a href="#">Point of using lorem ipsum </a></h4>
                            <div class="text">Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model.</div>
                        </div>
                    </div>
                </div>
                
                <!--Featured Project Column-->
                <div class="featured-project-column col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="#"><img src="<?=base_url()?>assets/images/resource/featured-image-10.jpg" alt=""></a><a href="#" class="theme-btn link-btn">View Project</a></figure>
                        <div class="lower-content">
                            <h4><a href="#">Point of using lorem ipsum </a></h4>
                            <div class="text">Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model.</div>
                        </div>
                    </div>
                </div>
                
                <!--Featured Project Column-->
                <div class="featured-project-column col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="#"><img src="<?=base_url()?>assets/images/resource/featured-image-11.jpg" alt=""></a><a href="#" class="theme-btn link-btn">View Project</a></figure>
                        <div class="lower-content">
                            <h4><a href="#">Point of using lorem ipsum </a></h4>
                            <div class="text">Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model.</div>
                        </div>
                    </div>
                </div>
                
                <!--Featured Project Column-->
                <div class="featured-project-column col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="#"><img src="<?=base_url()?>assets/images/resource/featured-image-12.jpg" alt=""></a><a href="#" class="theme-btn link-btn">View Project</a></figure>
                        <div class="lower-content">
                            <h4><a href="#">Point of using lorem ipsum </a></h4>
                            <div class="text">Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model.</div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="text-center padd-top-40"><a href="blog.html" class="theme-btn btn-style-three">Load More</a></div>
        </div>
    </section>