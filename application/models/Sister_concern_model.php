<?php
/**
 * @author Cloud Next generation <rjs.jahid11@gmail.com>
 * 
 * @copyright  Copyright (C) 2017 Cloud Next generation. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Sister_concern_model extends CI_Model {

    function __construct() {
        $this->table = 'sister_concern';
        parent :: __construct();
    }


    public function add($image) {

        // url generate 
        $name = $this->input->post('name');
        $x = trim($name);
        $x = strtolower($x);
        $x = str_replace('&', 'and', $x);
        $x = str_replace(' ', '_', $x);
        $x = str_replace('-', '_', $x);
        $url = str_replace('.', '', $x);


        $value = array(

            'name'=> $this->input->post('name'),
            'short_desc'=> $this->input->post('short_desc'),
            'long_desc'=> $this->input->post('long_desc'),
            'url'=> $url,
            'image'=> $image

        );
        
        $result = $this->data->save($this->table, $value);

         if($result){

            return TRUE;

         }  else {

            return FALSE; 

         }
        
    }
    
    public function edit($id, $image = NULL) {

        // url generate 
        $name = $this->input->post('name');
        $x = trim($name);
        $x = strtolower($x);
        $x = str_replace('&', 'and', $x);
        $x = str_replace(' ', '_', $x);
        $x = str_replace('-', '_', $x);
        $url = str_replace('.', '', $x);

        $value = array(

            'name'=> $this->input->post('name'),
            'short_desc'=> $this->input->post('short_desc'),
            'long_desc'=> $this->input->post('long_desc'),
            'url'=> $url,
            'image'=> $image

        );

        $result = $this->data->update($this->table, $id, $value);

        if($result){

            return TRUE;

        }  else {

            return FALSE; 

        }
    }


}