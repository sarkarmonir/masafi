<?php
/**
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 * 
 * @copyright  Copyright (C) 2017 rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This controller used for customer maintains
 * @package customer
 * @author Jahid Al Mamun
 */
class Sister_concern_info_model
 extends CI_Model {

    /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        $this->table = 'sister_concern_info';
        parent :: __construct();
    }


    /**
     * Add New customer in database table customers
     *@author Jahid Al Mamun
     * @param bool tru/false
     */
    public function add($vision_image, $mission_image, $core_image, $corporate_image) {
        
        $value = array(

            'url'=> $this->input->post('url'),

            'about_desc'=> $this->input->post('about_desc'),
            'about_video'=> $this->input->post('about_video'),

            'vision_image'=> $vision_image,
            'vision_desc'=> $this->input->post('vision_desc'),

            'mission_image'=> $mission_image,
            'mission_desc'=> $this->input->post('mission_desc'),

            'core_image'=> $core_image,
            'core_desc'=> $this->input->post('core_desc'),

            'corporate_image'=> $corporate_image,
            'corporate_desc'=> $this->input->post('corporate_desc')

        );
        
        $result = $this->data->save($this->table, $value);

         if($result){

            return TRUE;

         }  else {

            return FALSE; 

         }
        
    }
    
    public function edit($id, $vision_image = NULL, $mission_image = NULL, $core_image = NULL, $corporate_image = NULL) {

        $value = array(

            'url'=> $this->input->post('url'),

            'about_desc'=> $this->input->post('about_desc'),
            'about_video'=> $this->input->post('about_video'),

            'vision_image'=> $vision_image,
            'vision_desc'=> $this->input->post('vision_desc'),

            'mission_image'=> $mission_image,
            'mission_desc'=> $this->input->post('mission_desc'),

            'core_image'=> $core_image,
            'core_desc'=> $this->input->post('core_desc'),

            'corporate_image'=> $corporate_image,
            'corporate_desc'=> $this->input->post('corporate_desc')
        );

        $result = $this->data->update($this->table, $id, $value);

        if($result){

            return TRUE;

        }  else {

            return FALSE; 

        }

    }


    public function check_exist_url($url){

        $r = $this->db->select('url')->from($this->table)->where('url', $url)->get();

        return $url = $r->row();

    }
    
    public function get_details_by_url($url){

        $r = $this->db->select('*')->from($this->table)->where('url', $url)->get();

        return $url = $r->row();
        
    }


}
