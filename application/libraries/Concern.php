<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Concern {

    public function __construct() {

        $CI = & get_instance();

        $r = $CI->db->select("name, url")->get("sister_concern");

        $this->menu = $r->result();
    }  
}

?>